const http = require('http')

// POC

let SERVERS = []
if (process.env['OK']) {
  SERVERS = [
    {
      'status': 'on',
      'server_id': '555000',
      'user_id': '39259',
      'name': 'WindaubeGs',
      'created_at': '2018-10-23T14:07:56',
      'hostname': 'DESKTOP-89EV9F7',
      'updated_at': '2018-10-23T14:08:04',
      'handler_id': '6acdd0d2-960e-4b43-86d4-669123eab000',
      'yours': true,
      'version': 148,
      'build': '148-0',
      'owner': 'MYDIH',
      'id': 555000
    },
    {
      'status': 'on',
      'server_id': '556000',
      'user_id': '39259',
      'name': 'Tarst',
      'created_at': '2018-10-23T14:07:56',
      'hostname': 'DESKTOP-89EV9F7',
      'updated_at': '2018-10-23T14:08:04',
      'handler_id': '6acdd0d2-960e-4b43-86d4-669123eab000',
      'yours': true,
      'version': 148,
      'build': '148-0',
      'owner': 'MYDIH',
      'id': 556000
    }
  ]
}

http.createServer(async (req, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json' })
  res.end(JSON.stringify(SERVERS))
}).listen(9933)
