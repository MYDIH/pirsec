# Pirsec

This repo contains a UI written using SDL, to be used with the rapsberry pi, in order to run parsec client more easily. The project have simplicity in mind, you can only select the server you want to stream and start the parsec client to stream the server to the PI.

![](https://gitlab.com/MYDIH/pirsec/raw/dev/ScreenCast.webm)

*/!\ WARNING : This is a Work In Progress, some bugs are known, but I'm ok with them. They are quite hard to solve (some of them) and if nobody else is using this, I will not correct them. Don't hesitate to do PRs, or to make me know, one way or another, that this thing is used (stars/[issue](https://gitlab.com/MYDIH/pirsec/issues/1)).*

### Main features

* **Attractive UI**: Just because I think it matters.
* **Server selection**: You can select the server you want to stream.
* **Screensaver**: You can let your PI powered on without damaging your tv.
* **Full controller support**: Everything can be done with a game controller.
* **Shortcuts/Mouse support**: You can also use your mouse and/or your keyboard.
* **Wake support**: You can wake a computer (WakeOnLan or anything else).
* **Raspbian lite images available**: You can use a prebuild image from the releases tags or build your own.

### Raspbian images

You can use a prebuilt image from one of the tags. You can also build your own using the BuildImage.sh script (which essentially creates an Image folder which is a merged version of the pigen repo and the ImageOverrides, and just build using the docker ubuntu image cf : [pigen](https://github.com/RPi-Distro/pi-gen)). Be sure to have properly initialized the pi-gen submodule before trying to build the image.

*/!\ Be aware that building an image is quite long (~40m)*

#### Installation and Getting started.

Just use [etcher](https://www.balena.io/etcher/) to burn the image to an SDCard. On first launch, you'll need to have a keyboard connected in order to connect to parsec. The polling interval and the memory split is already configured.

On first run, raspi-config will launch in order to let you configure your keymap/locale/wifi country. When you are done, you should reboot to see the UI (either by letting raspi-config reboot for you or by issuing a sudo reboot when you got your prompt back, after hitting the finish button). If you need to do extra configuration steps (like configuring the actual wifi connexion for example), you should do them before rebooting, answering no to raspi-config if it kindly asks you if you want to reboot. You can also call `parsec` and configure your connexion during this extra configuration step (see the below warning note)

*/!\ Because of the recent change in the loging mechanism of the client, you need to connect to both parsec (issuing the `parsec` command) and form pirsec itself. On first UI run, you should connect using your parsec credentials on the pirsec login page. The login screen is far from perfect and I never coded a cursor to show you the text field has the focus because if the screen is shown, then the field is automatically "focused" (actually, there is no such thing implemented (focus)). When done, hit ALT+X on your keyboard in order to access the underlying tty. Connect to parsec issuing the `parsec` command from the prompt (you may need to issue it multiple times). After that, issue the `pirsec` command to get back to the UI.*

### Install from source

*/!\ Be aware that the compilation takes several minutes to complete on a pi 3b+*

If you want to install from source, just copy a release bundle or clone this repo to your pi, and issue thoses commands :

```
cmake CMakeLists.txt
make -j 4
```

You can then install this folder anywhere you want, updating the launcher's install_dir accordingly. You may want to soft link it to /usr/bin

```
ln -s <path_to_pirsecdir>/Pirsec.sh /usr/bin/pirsec
```

Issuing the `pirsec` command from anywhere should launch pirsec as expected.

You need to have the SDL2, SDL2_image and SDL2_ttf installed, along with libcurl4-gnutls-dev for curl support, and xboxdrv for controller support. You can learn how to install those from the "ImageOverrides/stage2/04-install-decent-SDL2".

You can obviously run this from any linux environnement which happens to have the required library installed. The build instructions are the same.

### Game Controllers

You can use your game controllers to control the UI. Here is the bindings applied :

|Button|Hooks|
|:----:|:----|
|**A**|Show help|
|**B**|Poweroff|
|**Y**|Reboot|
|**DOWN+Y**|Restart|
|**X**|Exit Pirsec (accessing the command line)|
|**START**|Start the stream on the specified server|
|**BACK**|Wake a server (using the Wake.sh script in Hooks folder)|
|**UP**|Change background|
|**LB/RB**|Previous/Next server|

### Shortcuts

You can also use your keyboard :

|Button|Hooks|
|:----:|:----|
|**ALT-P**|Poweroff|
|**ALT-R**|Reboot|
|**ALT-SHIFT-R**|Restart Pirsec|
|**ALT-X**|Exit Pirsec (accessing the command line)|
|**ALT-S**|Start the stream on the specified server|
|**ALT-W**|Wake a server (using the Wake.sh script in Hooks folder)|
|**ALT-B**|Change background|
|**ALT-LEFT/ALT-RIGHT**|Previous/Next server|

### Wake

When you use the wake feature using pirsec, it starts the script in Hooks/Wake.sh. This script should issue a start command and return immediately. Pirsec will then forbid screen saving and show an icon in the top left corner of the screen until the Servers list mutates on Parsec side.

### Themes

You can create a "Resources/Themes" folder and place exactly 30 images, in png format, and preferably with an alpha channel, pirsec will use those images as backgrounds instead of performing requests to the parsec servers. This helps mitigating one of the "RPI Quirks".

### RPI Quirks

This project encoutered numerous problems related to the raspberry pi environnement (raspbian lite). I will list them here, with their solution (which may not be ideal for some of them) :

* Raspbian doesn't have an X11 free SDL2 in it's repositories (as far as I know). This means that you need to install SDL2 from source. You can get an example and a list of libraries/options needed in the "Image/stage2/04-install-decent-SDL2"
* SDL2_image and SDL2_ttf are also needed. I didn't find a package that suits my need for both of them, but maybe it's possible. Anyway you can install those in the same fashion that the SDL2, by building from source. You also got an example in the "Image/stage2/04-install-decent-SDL2" folder. Be aware that in order to install the SDL2_ttf library, I needed to alter the Makefile to explicitly avoid linking to opengl. You can get a patched SDL2_ttf library in the "ImageOverrides/stage2/04-install-decent-SDL2/files" folder.
* The default curl package in the repositories of raspbian (libcurl4-openssl-dev) is giving segfaults. You can avoid those segfaults by using the libcurl4-gnutls-dev version.
* The SDL2_image IMG_LoadTextureRaw call is giving segfault on internal errors. The function that loads from file do not. This avoid us to do fancy things such as loading the backgrounds directly in memory instead of creating a temporary file in the /tmp folder. This is related to the next quirk
* The libjpeg library in the raspbian repositories is unable to load some images. I tried to rebuild the library from source without luck. This may be related to an underlying library (zlib or other). Anyway this means that a lot of backgrounds from the parsec servers (which are in jpeg) wont load. You can use a "Themes" folder to solve this.
* The SDL2 on the rapsberry pi uses a special driver which directly writes to the framebuffer, using some dispmanx and VC calls. This driver is not capable of hiding a window (it's implemented as if the SDL applications could only run one and only one window). This means that when the Pirsec window is shown, it hides the Parsec one. The way this quirk is managed is to close the window when parsec is properly lauched. However, I patched the SDL2 to include some dispmanx layer magic to being able to hide the window. This didn't work as is, and I didn't took the time to debug it. However any PR would be appreciated on this one because it could solve the next quirk.
* Closing the window can lead to a shift in the map of images (don't ask me why). This means that you can observe some misplaced images when you return from a parsec session. This only happens on the raspberry. I suspect it's linked to the standard library version or the gcc version. The only way to solve this at this time is to restart Pirsec. You can restart with ALT-SHIFT-R on a keyboard, by pressing DOWN+Y when you only have a controller, or by right clicking the reboot icon (for this to work, pirsec needs to be launched using the launcher (Pirsec.sh)).
* The pigen repository used to build the raspbian lite image is powerfull and highly configurable, but I found that the build process may fail for a lot of reasons. One of them is because you didn't modprobed the loop module for example (from the host). Make sure you red the pigen doc if you want to build the image by yourself.
* Last but not least, the SDL2 do not take locale (or keymap) into account. Dear french AZERTY users (like me), remember it when using keyboard shortcuts.
