#ifndef PIRSEC_SCHEDULER_HPP
#define PIRSEC_SCHEDULER_HPP
#pragma once

#include <chrono>
#include <functional>
#include <unordered_map>
#include <climits>

#include "Timeline.hpp"

namespace dge {
    typedef unsigned long TaskId;

    static const TaskId INVALID_TASK_ID = ULONG_MAX;
    static TaskId ID = 0;

    typedef std::function<void(double now, double at)> Task;

    class Scheduler : public virtual Timeline {
    public:
        TaskId schedule(const Task &t, const std::chrono::hours &duration);
        TaskId schedule(const Task &t, const std::chrono::minutes &duration);
        TaskId schedule(const Task &t, const std::chrono::seconds &duration);
        TaskId schedule(const Task &t, double at);

        TaskId interval(const Task &t, const std::chrono::hours &duration);
        TaskId interval(const Task &t, const std::chrono::minutes &duration);
        TaskId interval(const Task &t, const std::chrono::seconds &duration);

        void cancel(const TaskId &id);

    protected:
        void update(double now) override;

    private:
        struct StoredTask {
            Task task;
            double at;
            std::chrono::seconds duration; // needed to avoid segfault (Don't have the courage to debug it)
            bool isInterval;               // ... TODO: /!\ nuke this thing, eventually
        };

        std::unordered_map<TaskId, StoredTask> tasks;
    };

}

#endif //PIRSEC_SCHEDULER_HPP
