#pragma once

#include <vector>

namespace dge {

    class Timeline {
    public:
        static double getCurrentTime();

        void activate(Timeline *parent);

        virtual void restart();

        void pause();
        virtual void pause(double at);

        void tick();

        virtual ~Timeline();

    protected:
        virtual void update(double now);

    private:
        std::vector<Timeline *> m_responders;
        Timeline *m_parent = nullptr;
        double m_stoppedAt = 0;

        void registerAResponder(Timeline *responder);
        void unregisterAResponder(Timeline *responder);
    };

}