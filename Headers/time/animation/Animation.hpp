#pragma once

#include <cmath>
#include <cfloat>
#include <functional>

#include "../Timeline.hpp"

namespace dge {

    ///////////////////////////////////////////////////// RESPONDERS ///////////////////////////////////////////////////

    typedef std::function<void()> EventResponder;

    void DO_NOTHING();;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class Animation : public virtual Timeline {
    public:
        class RESTART {};
        class START {
        public:
            START() {}
            START(double at) : m_at(at) {}
            double m_at = -DBL_MAX;
        };
        class PAUSE {
        public:
            PAUSE() {}
            PAUSE(double at) : m_at(at) {}
            double m_at = -DBL_MAX;
        };
        class STOP {
        public:
            STOP() {}
            STOP(double at) : m_at(at) {}
            double m_at = -DBL_MAX;
        };
        class DURATION {
        public:
            DURATION(double duration) : m_duration(duration) {}
            double m_duration = 0;
        };
        class ON_FINISHED {
        public:
            ON_FINISHED(const EventResponder &fct) : m_fct(fct) {}
            const EventResponder &m_fct;
        };
        class ON_STOPPED {
        public:
            ON_STOPPED(const EventResponder &fct) : m_fct(fct) {}
            const EventResponder &m_fct;
        };
        class RUNNING {
        public:
            RUNNING(const std::function<void()> &runOnTrue) : m_func(runOnTrue) {}
            RUNNING(const std::function<void()> &runOnTrue, double at) : m_func(runOnTrue), m_at(at) {}
            const std::function<void()> &m_func;
            double m_at = -DBL_MAX;
        };
        class PAUSED {
        public:
            PAUSED(const std::function<void()> &runOnTrue) : m_func(runOnTrue) {}
            const std::function<void()> &m_func;
        };

        void restart() override;
        void start();
        void start(double at);
        using Timeline::pause;
        void pause(double at) override;
        void stop();
        void stop(double at);

        void setDuration(double newDuration);

        bool isRunning() const;
        bool isRunning(double at) const;
        bool isPaused() const;

        void animate(const RESTART &r);
        void animate(const START &s);
        void animate(const PAUSE &p);
        void animate(const STOP &s);
        void animate(const DURATION &d);
        void animate(const ON_FINISHED &f);
        void animate(const ON_STOPPED &f);
        void animate(const RUNNING &r);
        void animate(const PAUSED &p);

    protected:
        Animation(double duration = 1.0, bool loop = false);
        Animation(const Animation &a);

        virtual bool restartIfLoop(double now);
        virtual void run(double now);

        void update(double now) override;

        EventResponder m_finished = DO_NOTHING;
        EventResponder m_stopped = DO_NOTHING;
        double m_stopAfter = 0;
        double m_startAt = 0;
        double m_pauseAt = 0;

    private:
        bool m_realLoop = false;
        bool m_started = false;
        bool m_loop = false;
    };

}

