#pragma once

#include <math.h>

#include "Animation.hpp"

namespace dge {

    //////////////////////////////////////////////// INTERPOLATION FUNCTIONS ///////////////////////////////////////////

    typedef std::function<double(double x, double t, double b, double c, double d)> Easing;

    double EASE_LINEAR(double x, double t, double b, double c, double d);
    double EASE_SWING(double x, double t, double b, double c, double d);
    double EASE_IN_QUAD(double x, double t, double b, double c, double d);
    double EASE_OUT_QUAD(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_QUAD(double x, double t, double b, double c, double d);
    double EASE_IN_CUBIC(double x, double t, double b, double c, double d);
    double EASE_OUT_CUBIC(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_CUBIC(double x, double t, double b, double c, double d);
    double EASE_IN_QUART(double x, double t, double b, double c, double d);
    double EASE_OUT_QUART(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_QUART(double x, double t, double b, double c, double d);
    double EASE_IN_QUINT(double x, double t, double b, double c, double d);
    double EASE_OUT_QUINT(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_QUINT(double x, double t, double b, double c, double d);
    double EASE_IN_SINE(double x, double t, double b, double c, double d);
    double EASE_OUT_SINE(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_SINE(double x, double t, double b, double c, double d);
    double EASE_IN_EXPO(double x, double t, double b, double c, double d);
    double EASE_OUT_EXPO(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_EXPO(double x, double t, double b, double c, double d);
    double EASE_IN_CIRC(double x, double t, double b, double c, double d);
    double EASE_OUT_CIRC(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_CIRC(double x, double t, double b, double c, double d);
    double EASE_IN_ELASTIC(double x, double t, double b, double c, double d);
    double EASE_OUT_ELASTIC(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_ELASTIC(double x, double t, double b, double c, double d);
    double EASE_IN_BACK(double x, double t, double b, double c, double d);
    double EASE_OUT_BACK(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_BACK(double x, double t, double b, double c, double d);
    double EASE_IN_BOUNCE(double x, double t, double b, double c, double d);
    double EASE_OUT_BOUNCE(double x, double t, double b, double c, double d);
    double EASE_IN_OUT_BOUNCE(double x, double t, double b, double c, double d);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class AnimatedValue : public Animation {
    public:
        class END_VALUE {
        public:
            END_VALUE(float newValue) : m_endValue(newValue) {}
            float m_endValue;
        };
        class START_VALUE {
        public:
            START_VALUE(float newValue) : m_startValue(newValue) {}
            float m_startValue;
        };
        class INTERPOLATE {
        public:
            INTERPOLATE(const Easing &fct) : m_fct(fct) {}
            const Easing &m_fct;
        };
        class VALUE {
        public:
            VALUE(const std::function<void(float)> &func) : m_func(func) {}
            const std::function<void(float)> &m_func;
        };

        AnimatedValue(float startValue = 0, float endValue = 0,
                      double duration = 0, const Easing &easing = EASE_LINEAR,
                      bool loop = false, bool pingPong = false);
        AnimatedValue(const AnimatedValue &a);

        float value() const;

    protected:
        void run(double now) override;

        bool restartIfLoop(double now) override;

    public:
        float getStartValue() const;
        float getEndValue() const;
        void setStartValue(float newStartValue);
        void setEasing(const Easing &easing);
        void setEndValue(float newEndValue);

        using Animation::animate;
        void animate(const END_VALUE &v);
        void animate(const START_VALUE &v);
        void animate(const INTERPOLATE &i);
        void animate(const VALUE &v);

    private:
        Easing _easing = EASE_LINEAR;
        bool _pingPong = false;
        float _startValue = 0;
        float _endValue = 0;
        float _val = 0;
    };

}