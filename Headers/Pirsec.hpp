#ifndef SDL2TEST_PIRSEC_H
#define SDL2TEST_PIRSEC_H
#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <unordered_map>
#include <climits>
#include <string>

#include "time/Scheduler.hpp"

#include "Screen.hpp"

class Pirsec : public dge::Scheduler, public ResourceHandler {
private:
    bool mRunning = true;
    bool mRestarting = false;
    bool mInitialized = true;
    bool mScreenSaveCanceled = false;
    bool dpaddown = false;

    std::string mToken = "";
    int mWidth = -1, mHeight = -1;
    dge::TaskId mHideCursorTask = dge::INVALID_TASK_ID;
    dge::TaskId mScreenSaveTask = dge::INVALID_TASK_ID;
    std::unique_ptr<SDL_Window, Utils::SDL_Deleter> mWindow;
    std::unique_ptr<SDL_Renderer, Utils::SDL_Deleter> mRenderer;
    std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> mHandCursor;
    std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> mArrowCursor;

    std::unordered_map<std::string, std::unique_ptr<Screen>> mScreens;
    std::unordered_map<std::string, std::unique_ptr<TTF_Font, Utils::SDL_Deleter>> mFonts;

    void setSize();

    void getCredentials();
    void saveCredentials(const std::string &token);
    void scheduleScreenSaver();

    void render();
    void handle();
    void reset();

public:
    Uint32 PIRSEC_EVENTS;

    enum {
        PIRSEC_EVENTS_GET_URL_RESULTS = ((Sint32)1),
        PIRSEC_EVENTS_GET_URL_IMG_RESULTS = ((Sint32)2),
        PIRSEC_EVENTS_GET_URL = ((Sint32)3),
        PIRSEC_EVENTS_GET_URL_IMG = ((Sint32)4),
        PIRSEC_EVENTS_STOP = ((Sint32)5)
    };

    enum Ressources {
        IMG_B = 1,
        IMG_Y = 2,
        IMG_X = 3,
        IMG_LB = 4,
        IMG_RB = 5,
        IMG_START = 6,
        IMG_PARSEC = 7,
        IMG_SERVERS = 8,
        IMG_EXPERT = 9,
        IMG_EXPERT_HOVERED = 10,
        IMG_POWEROFF = 11,
        IMG_POWEROFF_HOVERED = 12,
        IMG_REBOOT = 13,
        IMG_REBOOT_HOVERED = 14,
        IMG_RPIMORE = 15,
        IMG_HELP_REGULAR = 16,
        IMG_HELP_LARGE = 17,
        IMG_LEFT = 18,
        IMG_RIGHT = 19,
        IMG_PIRSEC_LOGO = 20,
        IMG_WAKING = 21
    };

    explicit Pirsec();

    void initialize();
    bool restarting();
    bool running();
    void run();

    int width();
    int height();

    void restart();
    void stop();

    const std::string &getToken();
    void setToken(const std::string &token);

    void pushPirsecEvent(Sint32 type, void *data1, void *data2) const;

    void removeCredentials();

    void erase();

    void disableScreenSaver();
    void enableScreenSaver();

    const std::unique_ptr<SDL_Renderer, Utils::SDL_Deleter>& renderer() const;
    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter>& handCursor() const;
    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter>& arrowCursor() const;
    const std::unique_ptr<TTF_Font, Utils::SDL_Deleter>& font(const std::string &id) const;
};


#endif //SDL2TEST_PIRSEC_H
