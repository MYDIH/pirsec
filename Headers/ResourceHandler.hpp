#ifndef PIRSEC_RESOURCES_HPP
#define PIRSEC_RESOURCES_HPP
#pragma once

#include <unordered_map>
#include <memory>
#include <string>

#include "Resource.hpp"

class ResourceHandler {
private:
    std::unordered_map<int, std::unique_ptr<Resource>> mResources;

public:
    const std::unique_ptr<Resource>& get(int index);
    ResourceHandler& set(int index, Resource *res);

    void load(Pirsec *p) const;
};


#endif //PIRSEC_RESOURCES_HPP
