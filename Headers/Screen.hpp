#ifndef PIRSEC_SCREEN_HPP
#define PIRSEC_SCREEN_HPP
#pragma once

#include <SDL_events.h>

#include "ResourceHandler.hpp"
#include "Utils.hpp"

class Pirsec;

class Screen : public ResourceHandler {
protected:
    std::unordered_map<int, SDL_Rect> mBounds;
    bool mReset = true;

    virtual void layout(Pirsec *p);
    virtual void paint(Pirsec *p) = 0;

    bool hovering(int element, const SDL_Point &position);

public:
    enum Elements {
        ELEM_SCREEN = INT16_MAX
    };

    enum Resources {
        ANIM_SCREEN_OPACITY = INT16_MAX
    };

    Screen();

    virtual void initialize(Pirsec *p);

    virtual void handle(const SDL_Event &evt) { };
    void render(Pirsec *p);

    void reset();

    void show();
    void hide();

    bool isHidden();
    bool isShown();

    virtual ~Screen() = default;
};

#endif //PIRSEC_SCREEN_HPP
