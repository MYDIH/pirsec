#ifndef PIRSEC_UTILS_HPP
#define PIRSEC_UTILS_HPP
#pragma once


#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_ttf.h>

class Utils {
public:
    struct SDL_Deleter
    {
        void operator()(SDL_Window *p) const;
        void operator()(SDL_Renderer *p) const;
        void operator()(SDL_Texture *p) const;
        void operator()(SDL_Cursor *p) const;
        void operator()(SDL_GameController *p) const;
        void operator()(TTF_Font *p) const;
    };

    static size_t CURL_WRITE_TO_STRING_CALLBACK(char *contents, size_t size, size_t nmemb, void *userp);
    static size_t CURL_WRITE_TO_MEMORY_CALLBACK(char *contents, size_t size, size_t nmemb, void *userp);

    static void DrawCircle(SDL_Renderer *Renderer, int x, int y, int radius);
};


#endif //PIRSEC_UTILS_HPP
