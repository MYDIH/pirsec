#ifndef PIRSEC_TEXT_HPP
#define PIRSEC_TEXT_HPP
#pragma once

#include <SDL2/SDL_ttf.h>

#include "Resource.hpp"

// Yeah, the best way to handle this, is using some kind of glyph management and caching to be able to retrieve a glyph
// of any size anytime. I've implemented such a mechanism in dge:: but I will not propagate it here, we just have 3 lines
// of text to write ... Even PI will handle it.

class Text : public Resource {
private:
    std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> mTexture;
    std::string mText;
    TTF_Font *mFont;

public:
    Text(const std::string &text, TTF_Font *font);

    void load(Pirsec *p) override;

    const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &getTexture() override;
};


#endif //PIRSEC_TEXT_HPP
