#ifndef PIRSEC_MAIN_HPP
#define PIRSEC_MAIN_HPP
#pragma once

#include <SDL2/SDL_events.h>
#include <curl/curl.h>
#include <random>

#include "../Screen.hpp"
#include "../json.hpp"
#include "../time/Scheduler.hpp"

class Main : public Screen {
private:
    static std::string SERVERS_URL;
    static std::string BACKGROUNDS_URL;

    static std::string BACKGROUND_INDEX;

    const ushort START_MARGINS = 5;
    const ushort MAIN_MARGIN_TOP = 60;

    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &hand;
    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &arrow;

    SDL_Texture *background = nullptr;
    bool localBackground = false;
    std::string sessionId;
    int currentServer = 0;
    nlohmann::json servers;

    bool waking = false;
    bool first = true;
    Pirsec *pirsec;

    void showNoServer();
    void hideNoServer();

    void showHelp();
    void hideHelp();
    void toggleHelp();

    void setServers(const std::string &servers);
    void selectServer();

    void runHook(int hook, const std::string &args);

protected:
    void layout(Pirsec *p) override;
    void paint(Pirsec *p) override;

public:
    enum Hooks {
        START = 1,
        POWEROFF = 2,
        REBOOT = 3,
        WAKE = 4
    };

    enum Elements {
        ELEM_RPIMORE = 1,
        ELEM_REBOOT = 2,
        ELEM_POWEROFF = 3,
        ELEM_EXPERT = 4,
        ELEM_PARSEC = 5,
        ELEM_MAIN = 6,
        ELEM_MAIN_TEXT_LEFT = 7,
        ELEM_MAIN_TEXT_RIGHT = 8,
        ELEM_START = 9,
        ELEM_HELPX = 10,
        ELEM_X = 11,
        ELEM_HELPB = 12,
        ELEM_B = 13,
        ELEM_HELPY = 14,
        ELEM_Y = 15,
        ELEM_SERVERS = 16,
        ELEM_SERVERS_TEXT = 17,
        ELEM_LEFT = 18,
        ELEM_RIGHT = 19,
        ELEM_SERVERS_HELP = 20,
        ELEM_RB = 21,
        ELEM_LB = 22,
        ELEM_ERR_NOSERVERS = 23,
        ELEM_BACKGROUND = 24,
        ELEM_WAKING = 25,
    };

    enum Resources {
        ANIM_BACKGROUND_OPACITY = 1,
        ANIM_MAIN_OPACITY = 2,
        ANIM_EXPERT_HOVERED = 3,
        ANIM_POWEROFF_HOVERED = 4,
        ANIM_REBOOT_HOVERED = 5,
        ANIM_MAIN_TEXT = 6,
        ANIM_HELP_RPIMORE_OPACITY = 7,
        ANIM_HELP_SERVERS_OPACITY = 8,
        ANIM_HELP_Y = 9,
        ANIM_NO_SERVER_OPACITY = 10,
        ANIM_SERVERS_OPACITY = 11,
        TEXT_MAIN_LEFT = 12,
        TEXT_MAIN_RIGHT = 13,
        TEXT_SERVERS = 14,
        TEXT_ERR_NOSERVERS = 15
    };

    explicit Main(Pirsec *p);

    void handle(const SDL_Event &evt) override;

    void initialize(Pirsec *p) override;

    void fetchBackground();
    void fetchServers();
};

#endif // PIRSEC_MAIN_HPP
