#ifndef PIRSEC_SCREENSAVER_HPP
#define PIRSEC_SCREENSAVER_HPP

#include "../Screen.hpp"

class ScreenSaver : public Screen {
protected:
    void layout(Pirsec *p) override;

    void paint(Pirsec *p) override;

public:
    enum Elements {
        PIRSEC_LOGO = 1
    };

    void initialize(Pirsec *p) override;
};

#endif //PIRSEC_SCREENSAVER_HPP
