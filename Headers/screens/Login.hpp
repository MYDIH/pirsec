#ifndef PIRSEC_LOGIN_HPP
#define PIRSEC_LOGIN_HPP
#pragma once

#include <SDL_events.h>
#include <curl/curl.h>

#include "../Screen.hpp"
#include "../Text.hpp"

class Login : public Screen {
private:
    std::string text;
    std::unique_ptr<Text> written;
    bool renderText = false;
    bool password = false;

    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &hand;
    const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &arrow;
    Pirsec *pirsec;

    std::string username;
    std::string pwd;

    CURL *curlHandle;

    std::string hide(const std::string &str);

    std::string extractToken(const std::string &token);
    void onConfirm();
    void getToken();

protected:
    void layout(Pirsec *p) override;
    void paint(Pirsec *p) override;

public:
    enum Elements {
        ELEM_PARSEC = 1,
        ELEM_PROMPT = 3,
        ELEM_USERNAME = 4,
        ELEM_PASSWORD = 5,
        ELEM_CONFIRM = 6,
        ELEM_HELP = 7
    };

    enum Ressources {
        IMG_PROMPT = 1,
        TEXT_USERNAME = 2,
        TEXT_PASSWORD = 3,
        TEXT_HELP = 4
    };

    explicit Login(Pirsec *p);

    void handle(const SDL_Event &evt) override;
    void initialize(Pirsec *p) override;

    ~Login() override;
};

#endif // PIRSEC_MAIN_HPP
