#ifndef PIRSEC_Reboot_HPP
#define PIRSEC_Reboot_HPP
#pragma once

#include "../Image.hpp"

class Reboot : public Image {
public:
  Reboot();
};

#endif // PIRSEC_Reboot_HPP
