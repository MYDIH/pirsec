#ifndef PIRSEC_HELPLARGE_HPP
#define PIRSEC_HELPLARGE_HPP
#pragma once

#include "../Image.hpp"

class HelpLarge : public Image {
public:
    HelpLarge();
};

#endif // PIRSEC_HELPLARGE_HPP
