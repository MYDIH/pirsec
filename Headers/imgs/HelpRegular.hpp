#ifndef PIRSEC_HELPREGULAR_HPP
#define PIRSEC_HELPREGULAR_HPP
#pragma once

#include "../Image.hpp"

class HelpRegular : public Image {
public:
    HelpRegular();
};

#endif // PIRSEC_HELPREGULAR_HPP
