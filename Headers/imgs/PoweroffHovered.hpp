#ifndef PIRSEC_PoweroffHovered_HPP
#define PIRSEC_PoweroffHovered_HPP
#pragma once

#include "../Image.hpp"

class PoweroffHovered : public Image {
public:
  PoweroffHovered();
};

#endif // PIRSEC_PoweroffHovered_HPP
