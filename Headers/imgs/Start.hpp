#ifndef PIRSEC_Start_HPP
#define PIRSEC_Start_HPP
#pragma once

#include "../Image.hpp"

class Start : public Image {
public:
  Start();
};

#endif // PIRSEC_Start_HPP
