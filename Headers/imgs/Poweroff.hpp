#ifndef PIRSEC_Poweroff_HPP
#define PIRSEC_Poweroff_HPP
#pragma once

#include "../Image.hpp"

class Poweroff : public Image {
public:
  Poweroff();
};

#endif // PIRSEC_Poweroff_HPP
