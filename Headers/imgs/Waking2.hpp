#ifndef PIRSEC_WAKING2_HPP
#define PIRSEC_WAKING2_HPP
#pragma once

#include "../Image.hpp"

class Waking2 : public Image {
public:
    Waking2();
};

#endif // PIRSEC_WAKING2_HPP
