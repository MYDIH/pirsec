#ifndef PIRSEC_RebootHovered_HPP
#define PIRSEC_RebootHovered_HPP
#pragma once

#include "../Image.hpp"

class RebootHovered : public Image {
public:
  RebootHovered();
};

#endif // PIRSEC_RebootHovered_HPP
