#ifndef PIRSEC_WAKING1_HPP
#define PIRSEC_WAKING1_HPP
#pragma once

#include "../Image.hpp"

class Waking1 : public Image {
public:
    Waking1();
};

#endif // PIRSEC_WAKING1_HPP
