#ifndef PIRSEC_PROMPT_HPP
#define PIRSEC_PROMPT_HPP
#pragma once

#include "../Image.hpp"

class Prompt : public Image {
public:
    Prompt();
};

#endif // PIRSEC_PROMPT_HPP
