#ifndef PIRSEC_PIRSECLOGO_HPP
#define PIRSEC_PIRSECLOGO_HPP
#pragma once


#include "../Image.hpp"

class PirsecLogo : public Image {
public:
    PirsecLogo();
};


#endif //PIRSEC_PIRSECLOGO_HPP
