#ifndef PIRSEC_RESOURCE_HPP
#define PIRSEC_RESOURCE_HPP
#pragma once

#include <SDL2/SDL_render.h>
#include <memory>

#include "../Headers/Utils.hpp"
#include "time/animation/AnimatedValue.hpp"

class Pirsec;

class Resource {
public:
    virtual void load(Pirsec *p) = 0;

    virtual const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter>& getTexture() { throw std::runtime_error("Calling an abstract class, check the resource's type you try to access"); }
    virtual dge::AnimatedValue &getAnimatedValue() { throw std::runtime_error("Calling an abstract class, check the resource's type you try to access"); }

    virtual ~Resource() = default;
};

#endif //PIRSEC_RESOURCE_HPP
