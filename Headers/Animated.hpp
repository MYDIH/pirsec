#ifndef PIRSEC_ANIMATED_HPP
#define PIRSEC_ANIMATED_HPP
#pragma once

#include "Resource.hpp"
#include "time/animation/AnimatedValue.hpp"

class Animated : public Resource, public dge::AnimatedValue {
public:
    Animated(float startValue = 0, float endValue = 0,
                 double duration = 0, const dge::Easing &easing = dge::EASE_LINEAR,
                 bool loop = false, bool pingPong = false);
    Animated(const AnimatedValue &a);

    void load(Pirsec *p) override;

    AnimatedValue & getAnimatedValue() override;
};

#endif //PIRSEC_ANIMATED_HPP
