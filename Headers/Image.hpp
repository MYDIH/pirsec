#ifndef ABSTRACT_IMAGE_H
#define ABSTRACT_IMAGE_H
#pragma once

#include <SDL2/SDL_render.h>
#include <string>

#include "Resource.hpp"

class Image : public Resource {
private:
    std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> mTexture;
    std::string mPath;

public:
    explicit Image(const std::string &path);

    void load(Pirsec *r) override;
    const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &getTexture() override;

    const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &texture();
    const std::string &path();
};

#endif
