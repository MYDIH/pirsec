#ifndef PIRSEC_RUNNER_HPP
#define PIRSEC_RUNNER_HPP
#pragma once

#include <SDL2/SDL_quit.h>

#include "Pirsec.hpp"

class Runner {
public:
    static void run(Pirsec *p);
};


#endif //PIRSEC_RUNNER_HPP
