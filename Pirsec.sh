#!/bin/bash

INSTALL_DIR=/opt/Pirsec

current=$(pwd)
cd $INSTALL_DIR

mkdir -p ~/.pirsec

clear

./Pirsec "$@"
while [ "$?" -eq 42 ]; do
    ./Pirsec "$@"
done
