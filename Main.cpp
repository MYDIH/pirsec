#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <curl/curl.h>
#include <thread>

#include "Headers/Image.hpp"
#include "Headers/ResourceHandler.hpp"
#include "Headers/Pirsec.hpp"
#include "Headers/Runner.hpp"

void ExitHandler() {
    curl_global_cleanup();
    TTF_Quit();
    SDL_Quit();
}

int main(int argc, char *argv[]) {
    bool restart;

    // Initialize SDL.
    atexit(ExitHandler);
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0 || TTF_Init() == -1) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return 1;
    }

    curl_global_init(CURL_GLOBAL_NOTHING);

    // Using the heap here is mandatory to make sure we can delete the Pirsec app
    // to close the window cleanly (and avoid hanging for a seconds)
    auto *p = new Pirsec();
    std::thread longRunner(Runner::run, p);
    while(p->running()) {
        p->run();
    }
    restart = p->restarting();

    // Push the close event to the threads
    p->pushPirsecEvent(p->PIRSEC_EVENTS_STOP, NULL, NULL);
    // close the window
    delete p;

    // Wait for background thread to complete
    longRunner.join();

    return ((restart) ? 42 : 0);
}
