#!/bin/bash

# clean the build env
rm -rf Image/

# Add Pirsec distribution code
./package
cp Pirsec.tar.gz ImageOverrides/stage2/05-install-pirsec-ui/files/

# Copy pi-gen directory which points to the repo.
# Don't forget to keep this as updated as possible (using git submodule comamnds).
mkdir -p Image/
cp -r pi-gen/* Image/
# The git repository is used in the build script
# cp -r pi-gen/.git Image/
# Because the upper command isn't resolving the issue
sed -i -e 's/GIT_HASH=$(git rev-parse HEAD)/GIT_HASH='"$(git rev-parse HEAD)"'/g' Image/scripts/common

# Patch the build environment
cp -r ImageOverrides/* Image/
rm -rf Image/stage3 Image/stage4 Image/stage5

cd Image/
CONTINUE=1 PRESERVE_CONTAINER=1 ./build-docker.sh
