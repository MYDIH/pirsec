#include "../../Headers/time/Timeline.hpp"

#include <chrono>
#include <algorithm>
#include <iostream>

namespace dge {

    double Timeline::getCurrentTime() {
        std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()
        );
        return (double) (ms.count()) / 1000;
    }

    void Timeline::restart() {
        m_stoppedAt = 0;
    }

    void Timeline::pause()
    { pause(getCurrentTime()); }

    void Timeline::pause(double at) {
        if (m_stoppedAt == 0)
            m_stoppedAt = at;
    }

    void Timeline::tick() {
        double now = getCurrentTime();

        if (m_stoppedAt != 0)
            now = m_stoppedAt;

        update(now);
    }

    void Timeline::activate(Timeline *parent) {
        if (parent != nullptr) {
            m_parent = parent;
            parent->registerAResponder(this);
        }
    }

    void Timeline::registerAResponder(Timeline *responder) {
        m_responders.push_back(responder);
    }

    void Timeline::unregisterAResponder(Timeline *responder) {
        m_responders.erase(std::remove(m_responders.begin(), m_responders.end(), responder), m_responders.end());
    }

    void Timeline::update(double now) {
        for (Timeline *responder : m_responders)
            responder->update(now);
    }

    Timeline::~Timeline() {
        if(m_parent != nullptr)
            m_parent->unregisterAResponder(this);
    }

}