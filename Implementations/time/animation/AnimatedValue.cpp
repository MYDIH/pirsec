#include "../../../Headers/time/animation/AnimatedValue.hpp"

namespace dge {

    /////////////////////////////////////////////////// EASINGS ////////////////////////////////////////////////////////

    // TODO : Implement proper and TESTED easings, those aren't working as expected, as far as I tested them.

    /*
     * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
     *
     * Uses the built in easing capabilities added In jQuery 1.1
     * to offer multiple easing options
     *
     * TERMS OF USE - jQuery Easing
     *
     * Open source under the BSD License.
     *
     * Copyright Â© 2008 George McGinley Smith
     * All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without modification,
     * are permitted provided that the following conditions are met:
     *
     * Redistributions of source code must retain the above copyright notice, this list of
     * conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice, this list
     * of conditions and the following disclaimer in the documentation and/or other materials
     * provided with the distribution.
     *
     * Neither the name of the author nor the names of contributors may be used to endorse
     * or promote products derived from this software without specific prior written permission.
     *
     * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
     * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
     *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
     *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
     *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
     * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
     *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
     * OF THE POSSIBILITY OF SUCH DAMAGE.
     *
    */

    double EASE_LINEAR(double x, double t, double b, double c, double d)
    { return x; }

    double EASE_SWING(double x, double t, double b, double c, double d)
    { return 0.5 - cos(x * M_1_PI) / 2; }

    double EASE_IN_QUAD(double x, double t, double b, double c, double d) {
        t /= d;
        return c * t * t + b;
    }

    double EASE_OUT_QUAD(double x, double t, double b, double c, double d) {
        t /= d;
        return -c * t * (t - 2) + b;
    }

    double EASE_IN_OUT_QUAD(double x, double t, double b, double c, double d) {
        if ((t /= d/2) < 1) return c / 2 * t * t + b;
        --t;
        return -c / 2 * (t * (t - 2) - 1) + b;
    }

    double EASE_IN_CUBIC(double x, double t, double b, double c, double d) {
        t /= d;
        return c * t * t * t + b;
    }

    double EASE_OUT_CUBIC(double x, double t, double b, double c, double d) {
        t = t / d - 1;
        return c * (t * t * t + 1) + b;
    }

    double EASE_IN_OUT_CUBIC(double x, double t, double b, double c, double d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        t -= 2;
        return c/2*(t*t*t + 2) + b;
    }

    double EASE_IN_QUART(double x, double t, double b, double c, double d) {
        t /= d;
        return c*t*t*t*t + b;
    }

    double EASE_OUT_QUART(double x, double t, double b, double c, double d) {
        t = t / d - 1;
        return -c * (t*t*t*t - 1) + b;
    }

    double EASE_IN_OUT_QUART(double x, double t, double b, double c, double d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
        t -= 2;
        return - c / 2 * (t * t * t * t - 2) + b;
    }

    double EASE_IN_QUINT(double x, double t, double b, double c, double d) {
        t /= d;
        return c * t * t * t * t * t + b;
    }

    double EASE_OUT_QUINT(double x, double t, double b, double c, double d) {
        t = t / d - 1;
        return c*(t*t*t*t*t + 1) + b;
    }

    double EASE_IN_OUT_QUINT(double x, double t, double b, double c, double d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
        t -= 2;
        return c/2*(t * t * t * t * t + 2) + b;
    }

    double EASE_IN_SINE(double x, double t, double b, double c, double d) {
        return -c * cos(t / d * (M_1_PI / 2)) + c + b;
    }

    double EASE_OUT_SINE(double x, double t, double b, double c, double d) {
        return c * sin(t / d * (M_1_PI / 2)) + b;
    }

    double EASE_IN_OUT_SINE(double x, double t, double b, double c, double d) {
        return -c / 2 * (cos(M_1_PI * t / d) - 1) + b;
    }

    double EASE_IN_EXPO(double x, double t, double b, double c, double d) {
        return (t == 0) ? b : c * pow(2, 10 * (t / d - 1)) + b;
    }

    double EASE_OUT_EXPO(double x, double t, double b, double c, double d) {
        return (t == d) ? b + c : c * (-pow(2, -10 * t / d) + 1) + b;
    }

    double EASE_IN_OUT_EXPO(double x, double t, double b, double c, double d) {
        if (t == 0) return b;
        if (t == d) return b+c;
        if ((t /= d / 2) < 1) return c / 2 * pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-pow(2, -10 * --t) + 2) + b;
    }

    double EASE_IN_CIRC(double x, double t, double b, double c, double d) {
        t /= d;
        return -c * (sqrt(1 - t * t) - 1) + b;
    }

    double EASE_OUT_CIRC(double x, double t, double b, double c, double d) {
        t = t / d - 1;
        return c * sqrt(1 - t * t) + b;
    }

    double EASE_IN_OUT_CIRC(double x, double t, double b, double c, double d) {
        if ((t /= d / 2) < 1) return -c / 2 * (sqrt(1 - t * t) - 1) + b;
        t -= 2;
        return c / 2 * (sqrt(1 - t * t) + 1) + b;
    }

    double EASE_IN_ELASTIC(double x, double t, double b, double c, double d) {
        double s, p = 0, a = c;
        if (t == 0) return b; if ((t /= d) == 1) return b + c;  if (!p) p = d * .3;
        if (a < abs(c)) { a = c; s = p / 4; }
        else s = p / (2 * M_1_PI) * asin (c / a);
        t -= 1;
        return -(a * pow(2, 10 * t) * sin( (t * d - s) * (2 * M_1_PI) / p)) + b;
    }

    double EASE_OUT_ELASTIC(double x, double t, double b, double c, double d) {
        double s, p = 0, a = c;
        if (t == 0) return b; if ((t /= d) == 1) return b + c;  if (!p) p = d * .3;
        if (a < abs(c)) { a = c; s = p / 4; }
        else s = p / (2 * M_1_PI) * asin (c/a);
        return a * pow(2, -10 * t) * sin((t * d - s) * (2 * M_1_PI) / p) + c + b;
    }

    double EASE_IN_OUT_ELASTIC(double x, double t, double b, double c, double d) {
        double s = 1.70158, p = 0, a = c;
        if (t == 0) return b; if ((t /= d / 2) == 2) return b + c;  if (!p) p = d * (.3 * 1.5);
        if (a < abs(c)) { a = c; s = p / 4; }
        else s = p / (2 * M_1_PI) * asin(c / a);
        t -= 1;
        if (t < 1) return -.5 * (a * pow(2, 10 * t) * sin((t * d - s) * (2 * M_1_PI) / p )) + b;
        return a * pow(2, -10 * t) * sin((t * d - s) * (2 * M_1_PI) / p) * .5 + c + b;
    }

    double EASE_IN_BACK(double x, double t, double b, double c, double d) {
        double s = 1.70158;
        t /= d;
        return c * t * t * ((s + 1) * t - s) + b;
    }

    double EASE_OUT_BACK(double x, double t, double b, double c, double d) {
        double s = 1.70158;
        t = t / d - 1;
        return c * (t * t * ((s + 1) * t + s) + 1) + b;
    }

    double EASE_IN_OUT_BACK(double x, double t, double b, double c, double d) {
        double s = 1.70158 * 1.525;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * ((s + 1) * t - s)) + b;
        t -= 2;
        return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b;
    }

    double EASE_IN_BOUNCE(double x, double t, double b, double c, double d) {
        return c - EASE_OUT_BOUNCE(x, d - t, 0, c, d) + b;
    }

    double EASE_OUT_BOUNCE(double x, double t, double b, double c, double d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        } else if (t < (2/2.75)) {
            t -= 1.5 / 2.75;
            return c * (7.5625 * t * t + .75) + b;
        } else if (t < (2.5/2.75)) {
            t-= 2.25 / 2.75;
            return c * (7.5625 * t * t + .9375) + b;
        } else {
            t -= 2.625 / 2.75;
            return c * (7.5625 * t * t + .984375) + b;
        }
    }

    double EASE_IN_OUT_BOUNCE(double x, double t, double b, double c, double d) {
        if (t < d / 2) return EASE_IN_BOUNCE(x, t * 2, 0, c, d) * .5 + b;
        return EASE_OUT_BOUNCE(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    AnimatedValue::AnimatedValue(float startValue, float endValue,
                                 double duration, const Easing &easing,
                                 bool loop, bool pingPong) :
            Animation(duration, loop),
            _startValue(startValue),
            _pingPong(pingPong),
            _endValue(endValue),
            _easing(easing)
    { _val = startValue; }

    AnimatedValue::AnimatedValue(const AnimatedValue &a) :
            Animation(a),
            _startValue(a._startValue),
            _pingPong(a._pingPong),
            _endValue(a._endValue),
            _easing(a._easing)
    { _val = a._startValue; }

    float AnimatedValue::value() const
    { return _val; }

    void AnimatedValue::run(double now) {
        Animation::run(now);

        if (now < m_startAt)
            _val = _startValue;
        else if (now > m_startAt + m_stopAfter)
            _val = _endValue;
        else {
            auto percentage = (now - m_startAt) / m_stopAfter;
            auto val = (float) (_startValue + (_endValue - _startValue) *
                    _easing(percentage, m_stopAfter * percentage, 0, 1, m_stopAfter));
            if (_startValue > _endValue) {
                _val = std::max(val, _endValue);
            } else {
                _val = std::min(val, _endValue);
            }
        }
    }

    bool AnimatedValue::restartIfLoop(double now) {
        bool restarting = Animation::restartIfLoop(now);
        if (restarting && _pingPong) {
            float start = getStartValue();
            setStartValue(getEndValue());
            setEndValue(start);
        }
        return restarting;
    }

    float AnimatedValue::getStartValue() const
    { return _startValue; }

    float AnimatedValue::getEndValue() const
    { return _endValue; }

    void AnimatedValue::setStartValue(float newStartValue)
    { _startValue = newStartValue; }

    void AnimatedValue::setEasing(const Easing &easing)
    { _easing = easing; }

    void AnimatedValue::setEndValue(float newEndValue)
    { _endValue = newEndValue; }

    void AnimatedValue::animate(const AnimatedValue::END_VALUE &v)
    { setEndValue(v.m_endValue); }

    void AnimatedValue::animate(const AnimatedValue::START_VALUE &v)
    { setStartValue(v.m_startValue); }

    void AnimatedValue::animate(const AnimatedValue::INTERPOLATE &i)
    { setEasing(i.m_fct); }

    void AnimatedValue::animate(const VALUE &v)
    { v.m_func(value()); }

}