#include "../../../Headers/time/animation/Animation.hpp"

#include <cstdint>

namespace dge {

    /////////////////////////////////////////////////// EVENTS /////////////////////////////////////////////////////////

    void DO_NOTHING() {}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Animation::Animation(double duration, bool loop) :
            m_startAt(DBL_MAX),
            m_stopAfter(duration),
            m_realLoop(loop),
            m_loop(loop)
    {}

    Animation::Animation(const Animation &a) :
            m_stopAfter(a.m_stopAfter),
            m_realLoop(a.m_loop),
            m_loop(a.m_loop)
    {}

    bool Animation::restartIfLoop(double now) {
        if (m_realLoop && !isRunning(now) && now > m_startAt) {
            start(now);
            return true;
        }
        return false;
    }

    void Animation::run(double now)
    { restartIfLoop(now); }

    void Animation::update(double now) {
        if(m_started && now > m_startAt + m_stopAfter) {
            m_finished();
            m_started = false;
        }
        run(now);
    }

    bool Animation::isRunning() const
    { return isRunning(getCurrentTime()); }

    bool Animation::isRunning(double at) const
    { return ((at >= m_startAt && at <= m_startAt + m_stopAfter) && m_pauseAt == 0); }

    bool Animation::isPaused() const
    { return m_pauseAt != 0; }

    void Animation::restart()
    { start(); }

    void Animation::start()
    { start(getCurrentTime()); }

    void Animation::start(double at) {
        m_pauseAt = 0;
        m_startAt = at;
        m_started = true;
        m_realLoop = m_loop;
    }

    void Animation::pause(double at) {
        if (at >= m_startAt && at <= (m_startAt + m_stopAfter))
            m_pauseAt = at;
        m_realLoop = false;
    }

    void Animation::stop()
    { stop(getCurrentTime()); }

    void Animation::stop(double at) {
        if (at >= m_startAt)
            m_startAt -= m_stopAfter;
        m_realLoop = false;
        m_started = false;
        m_pauseAt = 0;
        m_stopped();
    }

    void Animation::setDuration(double newDuration)
    { m_stopAfter = newDuration; }

    void Animation::animate(const RESTART &r)
    { restart(); }

    void Animation::animate(const START &s) {
        if(s.m_at == -DBL_MAX)
            start();
        else
            start(s.m_at);
    }

    void Animation::animate(const PAUSE &p) {
        if(p.m_at == -DBL_MAX)
            pause();
        else
            pause(p.m_at);
    }

    void Animation::animate(const STOP &s) {
        if(s.m_at == -DBL_MAX)
            stop();
        else
            stop(s.m_at);
    }

    void Animation::animate(const DURATION &d)
    { setDuration(d.m_duration); }

    void Animation::animate(const Animation::ON_FINISHED &f)
    { m_finished = f.m_fct; }

    void Animation::animate(const Animation::ON_STOPPED &f)
    { m_stopped = f.m_fct; }

    void Animation::animate(const RUNNING &r) {
        if(r.m_at == -DBL_MAX) {
            if (isRunning())
                r.m_func();
        }
        else if (isRunning(r.m_at))
            r.m_func();
    }

    void Animation::animate(const PAUSED &p) {
        if(isPaused())
            p.m_func();
    }
}