#include "../../Headers/time/Scheduler.hpp"

dge::TaskId dge::Scheduler::schedule(const Task &t, const std::chrono::hours &duration) {
    return schedule(t, std::chrono::duration_cast<std::chrono::seconds>(duration));
}

dge::TaskId dge::Scheduler::schedule(const Task &t, const std::chrono::minutes &duration) {
    return schedule(t, std::chrono::duration_cast<std::chrono::seconds>(duration));
}

dge::TaskId dge::Scheduler::schedule(const Task &t, const std::chrono::seconds &duration) {
    return schedule(t, getCurrentTime() + duration.count());
}

dge::TaskId dge::Scheduler::schedule(const Task &t, double at) {
    StoredTask st = { t, at };
    tasks.insert(std::make_pair(ID, st));
    return ID++;
}

dge::TaskId dge::Scheduler::interval(const dge::Task &t, const std::chrono::hours &duration) {
    return interval(t, std::chrono::duration_cast<std::chrono::seconds>(duration));
}

dge::TaskId dge::Scheduler::interval(const dge::Task &t, const std::chrono::minutes &duration) {
    return interval(t, std::chrono::duration_cast<std::chrono::seconds>(duration));
}

dge::TaskId dge::Scheduler::interval(const dge::Task &t, const std::chrono::seconds &duration) {
    StoredTask st = { t, getCurrentTime() + duration.count(), duration, true };
    tasks.insert(std::make_pair(ID, st));
    return ID++;
}

void dge::Scheduler::cancel(const dge::TaskId &id)
{ tasks.erase(id); }

void dge::Scheduler::update(double now) {
    Timeline::update(now);
    for (auto it = tasks.begin(); it != tasks.end();) {
        StoredTask &st = it->second;
        if (now >= st.at) {
            st.task(now, st.at);
            if (st.isInterval) {
                st.at = now + st.duration.count();
                it++;
            } else {
                it = tasks.erase(it);
            }
        } else {
            ++it;
        }
    }
}
