#include "../Headers/Text.hpp"

#include "../Headers/Pirsec.hpp"

Text::Text(const std::string &text, TTF_Font *font) :
    mText(text), mFont(font) {}

void Text::load(Pirsec *p) {
    SDL_Surface *tmp = TTF_RenderText_Blended(mFont, mText.c_str(), { 169, 169, 169 });
    mTexture = std::unique_ptr<SDL_Texture, Utils::SDL_Deleter>(SDL_CreateTextureFromSurface(p->renderer().get(), tmp));
    SDL_FreeSurface(tmp);
}

const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &Text::getTexture()
{ return mTexture; }
