#include "../../Headers/screens/ScreenSaver.hpp"

#include <random>

#include "../../Headers/Pirsec.hpp"

void ScreenSaver::layout(Pirsec *p) {
    Screen::layout(p);

    int tlw = 0, tlh = 0;
    SDL_Texture *logo = p->get(Pirsec::IMG_PIRSEC_LOGO)->getTexture().get();
    SDL_QueryTexture(logo, nullptr, nullptr, &tlw, &tlh);

    std::random_device rd;
    std::mt19937 gen(rd());
    int maxX = p->width() - tlw, maxY = p->height() - tlh;
    std::uniform_int_distribution<int> disX(0, maxX);
    std::uniform_int_distribution<int> disY(0, maxY);

    /// Main
    SDL_Rect Logo = { disX(gen), disY(gen), tlw, tlh };

    mBounds[PIRSEC_LOGO] = Logo;
}

void ScreenSaver::paint(Pirsec *p) {
    auto screenOpacity = static_cast<Uint8>(get(Screen::ANIM_SCREEN_OPACITY)->getAnimatedValue().value());

    SDL_Renderer *renderer = p->renderer().get();

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, screenOpacity);
    SDL_RenderFillRect(renderer, &mBounds[ELEM_SCREEN]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_PIRSEC_LOGO)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(renderer, p->get(Pirsec::IMG_PIRSEC_LOGO)->getTexture().get(), nullptr, &mBounds[PIRSEC_LOGO]);
}

void ScreenSaver::initialize(Pirsec *p) {
    Screen::initialize(p);

    p->interval([this](double now, double at) {
        reset();
    }, std::chrono::seconds(10));
}
