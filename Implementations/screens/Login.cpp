#include "../../Headers/screens/Login.hpp"

#include <iostream>

#include "../../Headers/imgs/Prompt.hpp"
#include "../../Headers/Pirsec.hpp"
#include "../../Headers/json.hpp"

std::string Login::hide(const std::string &str) {
    std::string returned;
    for (int i = 0; i < str.size(); i++) {
        returned += "*";
    }
    return returned;
}

std::string Login::extractToken(const std::string &token) {
    nlohmann::json tokenJSON = nlohmann::json::parse(token);
    return tokenJSON["session_id"];
}

void Login::onConfirm() {
    if (text.length() > 0) {
        if (password) {
            pwd = text;
            text.clear();
            getToken();
        } else {
            username = text;
            password = true;
            text.clear();
        }
    }
}

void Login::getToken() {
    std::string tokenJson;
    std::string authJson = R"({"email": ")" + username + R"(", "password": ")" + pwd + R"("})";
    struct curl_slist *headers = nullptr;
    headers = curl_slist_append(headers, "Content-Type: application/json; charset=utf-8");
    curl_easy_setopt(curlHandle, CURLOPT_URL, "https://parsecgaming.com/v1/auth");
    curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, authJson.c_str());
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, Utils::CURL_WRITE_TO_STRING_CALLBACK);
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &tokenJson);
    CURLcode res = curl_easy_perform(curlHandle);
    if(res != CURLE_OK)
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
    curl_slist_free_all(headers);

    try {
        pirsec->setToken(extractToken(tokenJson));
    } catch (...) {
        password = false;
    }
}

void Login::layout(Pirsec *p) {
    Screen::layout(p);

    SDL_Texture *username = get(TEXT_USERNAME)->getTexture().get();
    SDL_Texture *password = get(TEXT_PASSWORD)->getTexture().get();
    SDL_Texture *help = get(TEXT_HELP)->getTexture().get();

    SDL_Rect Parsec = { p->width() / 2 - 125, p->height() / 2 - 104, 250, 208 };
    SDL_Rect Prompt = { p->width() / 2 - 175, Parsec.y + Parsec.h + 50, 350, 40 };

    int tuw = 0, tuh = 0, tpw = 0, tph = 0, thw = 0, thh = 0;
    SDL_QueryTexture(username, nullptr, nullptr, &tuw, &tuh);
    SDL_QueryTexture(password, nullptr, nullptr, &tpw, &tph);
    SDL_QueryTexture(help, nullptr, nullptr, &thw, &thh);
    SDL_Rect Username = { Prompt.x + 15, Prompt.y + (Prompt.h / 2 - tuh / 2), tuw, tuh };
    SDL_Rect Password = { Prompt.x + 15, Prompt.y + (Prompt.h / 2 - tph / 2), tpw, tph };
    SDL_Rect Confirm = { Prompt.x + Prompt.w - 20 - 15, Prompt.y + (Prompt.h / 2 - 10), 20, 20 };

    SDL_Rect Help = { p->width() / 2 - thw / 2, Prompt.y + Prompt.h + 35, thw, thh };

    mBounds[ELEM_PARSEC] = Parsec;
    mBounds[ELEM_PROMPT] = Prompt;
    mBounds[ELEM_USERNAME] = Username;
    mBounds[ELEM_PASSWORD] = Password;
    mBounds[ELEM_CONFIRM] = Confirm;
    mBounds[ELEM_HELP] = Help;
}

void Login::paint(Pirsec *p) {
    SDL_Renderer *r = p->renderer().get();
    int tww = 0, twh = 0;

    // Re-generate text if needed
    if (renderText && !text.empty()) {
        if(password) {
            written = std::unique_ptr<Text>(new Text(hide(text), p->font("RubikMain").get()));
        } else {
            written = std::unique_ptr<Text>(new Text(text, p->font("RubikMain").get()));
        }
        written->load(p); // Initialize directly
    }

    /////////////////////////////////////////////////// Opacity ////////////////////////////////////////////////////////
    auto screenOpacity = static_cast<Uint8>(get(Screen::ANIM_SCREEN_OPACITY)->getAnimatedValue().value());
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    SDL_SetRenderDrawColor(r, 37, 37, 63, screenOpacity);
    SDL_RenderFillRect(r, &mBounds[ELEM_SCREEN]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_PARSEC)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_PARSEC)->getTexture().get(), nullptr, &mBounds[ELEM_PARSEC]);
    SDL_SetTextureAlphaMod(get(IMG_PROMPT)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, get(IMG_PROMPT)->getTexture().get(), nullptr, &mBounds[ELEM_PROMPT]);

    if (!text.empty()) {
        SDL_QueryTexture(written->getTexture().get(), nullptr, nullptr, &tww, &twh);
        SDL_Rect Written = { mBounds[ELEM_USERNAME].x, mBounds[ELEM_USERNAME].y, tww, twh };
        SDL_Rect clipWritten = { mBounds[ELEM_USERNAME].x, mBounds[ELEM_USERNAME].y,
                                 mBounds[ELEM_CONFIRM].x - mBounds[ELEM_USERNAME].x - 15, twh };
        SDL_RenderSetClipRect(r, &clipWritten);
        SDL_SetTextureAlphaMod(written->getTexture().get(), screenOpacity);
        SDL_RenderCopy(r, written->getTexture().get(), nullptr, &Written);
        SDL_RenderSetClipRect(r, nullptr);
    } else {
        if (password) {
            SDL_SetTextureAlphaMod(get(TEXT_PASSWORD)->getTexture().get(), screenOpacity);
            SDL_RenderCopy(r, get(TEXT_PASSWORD)->getTexture().get(), nullptr, &mBounds[ELEM_PASSWORD]);
        } else {
            SDL_SetTextureAlphaMod(get(TEXT_USERNAME)->getTexture().get(), screenOpacity);
            SDL_RenderCopy(r, get(TEXT_USERNAME)->getTexture().get(), nullptr, &mBounds[ELEM_USERNAME]);
        }
    }
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_RIGHT)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_RIGHT)->getTexture().get(), nullptr, &mBounds[ELEM_CONFIRM]);

    SDL_SetTextureAlphaMod(get(TEXT_HELP)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, get(TEXT_HELP)->getTexture().get(), nullptr, &mBounds[ELEM_HELP]);
}

Login::Login(Pirsec *p) : pirsec(p), hand(p->handCursor()), arrow(p->arrowCursor()) {
    set(IMG_PROMPT, new Prompt());
    set(TEXT_USERNAME, new Text("Username", p->font("RubikMain").get()));
    set(TEXT_PASSWORD, new Text("Password", p->font("RubikMain").get()));
    set(TEXT_HELP, new Text("Those credentials will be asked only once in a while", p->font("RubikMain").get()));
}

void Login::handle(const SDL_Event &evt) {
    if (evt.type == SDL_MOUSEMOTION) {
        SDL_Point mousePosition = {evt.motion.x, evt.motion.y};
        bool confirmHovered = hovering(ELEM_CONFIRM, mousePosition);

        if (confirmHovered) {
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
        } else {
            if (SDL_GetCursor() != arrow.get())
                SDL_SetCursor(arrow.get());
        }
    } else if (evt.type == SDL_KEYDOWN) {
        switch (evt.key.keysym.sym) {
            case SDLK_RETURN:
                onConfirm();
                break;
            case SDLK_BACKSPACE:
                if (text.length() > 0) {
                    text.pop_back();
                    renderText = true;
                }
                break;
            case SDLK_c: // ctrl + c
                if (SDL_GetModState() & KMOD_CTRL) {
                    SDL_SetClipboardText(text.c_str());
                }
                break;
            case SDLK_v: // ctrl + v
                if (SDL_GetModState() & KMOD_CTRL) {
                    text = SDL_GetClipboardText();
                    renderText = true;
                }
                break;
            default:
                break;
        }
    } else if (evt.type == SDL_TEXTINPUT) {
        // Not copy or pasting
        if (!((evt.text.text[0] == 'c' || evt.text.text[0] == 'C') &&
              (evt.text.text[0] == 'v' || evt.text.text[0] == 'V') &&
               SDL_GetModState() & KMOD_CTRL)) {
            text += evt.text.text;
            renderText = true;
        }
    }
}

void Login::initialize(Pirsec *p) {
    Screen::initialize(p);
    curlHandle = curl_easy_init();
}

Login::~Login() {
    curl_easy_cleanup(curlHandle);
}
