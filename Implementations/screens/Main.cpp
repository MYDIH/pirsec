#include "../../Headers/screens/Main.hpp"

#include <SDL_image.h>
#include <sys/stat.h>
#include <iostream>

#include "../../Headers/Animated.hpp"
#include "../../Headers/Pirsec.hpp"
#include "../../Headers/Text.hpp"

std::string Main::BACKGROUNDS_URL("https://public.parsecgaming.com/community/ultros/$i/banner");
std::string Main::SERVERS_URL("https://parsecgaming.com/v1/server-list?include_managed=true");

// To use with the Server faker in the Test folder
// std::string Main::SERVERS_URL("http://localhost:9933");

std::string Main::BACKGROUND_INDEX("2");

// This mess would really benefit from an AnimationTimeline class
// TODO: Implement a timeline class

void Main::showNoServer() {
    dge::AnimatedValue &mainOpacity = get(ANIM_MAIN_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &serversOpacity = get(ANIM_SERVERS_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &noServerOpacity = get(ANIM_NO_SERVER_OPACITY)->getAnimatedValue();

    if (mainOpacity.getEndValue() != 0.0) {
        mainOpacity.setStartValue(mainOpacity.value());
        mainOpacity.setEndValue(0.0);
    }

    if (serversOpacity.getEndValue() != 0.0) {
        serversOpacity.setStartValue(serversOpacity.value());
        serversOpacity.setEndValue(0.0);
    }

    if (noServerOpacity.getEndValue() != 255.0) {
        noServerOpacity.setStartValue(noServerOpacity.value());
        noServerOpacity.setEndValue(255.0);
    }

    if (!mainOpacity.isRunning() && mainOpacity.value() != 0.0)
        mainOpacity.start();

    if (!serversOpacity.isRunning() && serversOpacity.value() != 0.0)
        serversOpacity.start();

    if (!noServerOpacity.isRunning() && noServerOpacity.value() != 255.0)
        noServerOpacity.start();
}

void Main::hideNoServer() {
    dge::AnimatedValue &noServerOpacity = get(ANIM_NO_SERVER_OPACITY)->getAnimatedValue();

    if (noServerOpacity.getEndValue() != 0.0) {
        noServerOpacity.setStartValue(noServerOpacity.value());
        noServerOpacity.setEndValue(0.0);
    }

    if (!noServerOpacity.isRunning() && noServerOpacity.value() != 0.0)
        noServerOpacity.start();
}

void Main::showHelp() {
    dge::AnimatedValue &helpRPIOpacity = get(ANIM_HELP_RPIMORE_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &helpServersOpacity = get(ANIM_HELP_SERVERS_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &helpY = get(ANIM_HELP_Y)->getAnimatedValue();

    if (helpRPIOpacity.getEndValue() != 255.0) {
        helpRPIOpacity.setStartValue(helpRPIOpacity.value());
        helpRPIOpacity.setEndValue(255.0);
    }

    if (helpServersOpacity.getEndValue() != 255.0) {
        helpServersOpacity.setStartValue(helpServersOpacity.value());
        helpServersOpacity.setEndValue(255.0);
    }

    if (helpY.getEndValue() != 0.0) {
        helpY.setStartValue(helpY.value());
        helpY.setEndValue(0.0);
    }

    if (!helpRPIOpacity.isRunning() && helpRPIOpacity.value() != 255.0)
        helpRPIOpacity.start();

    if (!helpServersOpacity.isRunning() && helpServersOpacity.value() != 255.0)
        helpServersOpacity.start();

    if (!helpY.isRunning() && helpY.value() != 0.0)
        helpY.start();
}

void Main::hideHelp() {
    dge::AnimatedValue &helpRPIOpacity = get(ANIM_HELP_RPIMORE_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &helpServersOpacity = get(ANIM_HELP_SERVERS_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &helpY = get(ANIM_HELP_Y)->getAnimatedValue();

    if (helpRPIOpacity.getEndValue() != 0.0) {
        helpRPIOpacity.setStartValue(helpRPIOpacity.value());
        helpRPIOpacity.setEndValue(0.0);
    }

    if (helpServersOpacity.getEndValue() != 0.0) {
        helpServersOpacity.setStartValue(helpServersOpacity.value());
        helpServersOpacity.setEndValue(0.0);
    }

    if (helpY.getEndValue() != 15.0) {
        helpY.setStartValue(helpY.value());
        helpY.setEndValue(15.0);
    }

    if (!helpRPIOpacity.isRunning() && helpRPIOpacity.value() != 0.0)
        helpRPIOpacity.start();

    if (!helpServersOpacity.isRunning() && helpServersOpacity.value() != 0.0)
        helpServersOpacity.start();

    if (!helpY.isRunning() && helpY.value() != 15.0)
        helpY.start();
}

void Main::toggleHelp() {
    dge::AnimatedValue &helpRPIOpacity = get(ANIM_HELP_RPIMORE_OPACITY)->getAnimatedValue();
    if (helpRPIOpacity.getEndValue() == 255.0) {
        hideHelp();
    } else {
        showHelp();
    }
}

void Main::setServers(const std::string &servers) {
    auto newServers = nlohmann::json::parse(servers);
    std::string serversList = newServers.dump();

    // std::cout << "Server list received : " << serversList << std::endl;

    if (serversList != this->servers.dump()) {
        this->servers = newServers;
        selectServer();
        waking = false;
        pirsec->enableScreenSaver();
    }
}

void Main::selectServer() {
    set(TEXT_SERVERS, new Text(servers[currentServer]["name"], pirsec->font("RubikMain").get()));
    get(TEXT_SERVERS)->load(pirsec);
    reset();
}

void Main::fetchServers() {
    if (sessionId.empty()) sessionId = "X-Parsec-Session-Id: " + pirsec->getToken();
    pirsec->pushPirsecEvent(Pirsec::PIRSEC_EVENTS_GET_URL, &SERVERS_URL, &sessionId);
}

void Main::fetchBackground() {
    struct stat statbuf;

    if (stat("Resources/Themes", &statbuf) != -1) {
        if (S_ISDIR(statbuf.st_mode)) {
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<short> dis(1, 30);

            BACKGROUND_INDEX = std::to_string(dis(gen));
            std::string filename = "Resources/Themes/" + BACKGROUND_INDEX + ".png";

            if (background != nullptr) SDL_DestroyTexture(background);
            background = IMG_LoadTexture(pirsec->renderer().get(), filename.c_str());

            if (background == nullptr) std::cout << "Texture can't be loaded : " << SDL_GetError() << std::endl;

            auto &backgroundAnimation = get(ANIM_BACKGROUND_OPACITY)->getAnimatedValue();
            backgroundAnimation.setStartValue(0.0);
            backgroundAnimation.setEndValue(40.0);
            backgroundAnimation.start();

            localBackground = true;
        }
    } else {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<short> dis(2, 53);

        BACKGROUND_INDEX = std::to_string(dis(gen));

        pirsec->pushPirsecEvent(Pirsec::PIRSEC_EVENTS_GET_URL_IMG, &BACKGROUNDS_URL, &BACKGROUND_INDEX);
    }
}

void Main::runHook(const int hook, const std::string &args) {
    switch (hook) {
        case START: {
            // std::cout << "START hook called." << std::endl;
            pirsec->erase();
            system(std::string("./Hooks/Start.sh ").append(args).c_str());
            if (background != nullptr) {
                SDL_DestroyTexture(background);
                background = nullptr;
            }
            fetchBackground();
            pirsec->initialize();
            break;
        } case POWEROFF: {
            // std::cout << "POWEROFF hook called." << std::endl;
            system(std::string("./Hooks/Poweroff.sh ").append(args).c_str());
            break;
        } case REBOOT: {
            // std::cout << "REBOOT hook called." << std::endl;
            system(std::string("./Hooks/Reboot.sh ").append(args).c_str());
            break;
        } case WAKE: {
            // std::cout << "WAKE hook called." << std::endl;
            system(std::string("./Hooks/Wake.sh ").append(args).c_str());
            waking = true;
            break;
        } default: {
            break;
        }
    }
}

void Main::layout(Pirsec *p) {
    Screen::layout(p);

    SDL_Texture *leftMainText = get(TEXT_MAIN_LEFT)->getTexture().get();
    SDL_Texture *rightMainText = get(TEXT_MAIN_RIGHT)->getTexture().get();
    SDL_Texture *noServerError = get(TEXT_ERR_NOSERVERS)->getTexture().get();

    /// Main
    SDL_Rect Parsec = { p->width() / 2 - 125, p->height() / 2 - 104, 250, 208 };

    int tlw = 0, tlh = 0, trw = 0, trh = 0;
    SDL_QueryTexture(leftMainText, nullptr, nullptr, &tlw, &tlh);
    SDL_QueryTexture(rightMainText, nullptr, nullptr, &trw, &trh);
    SDL_Rect TextLeft = { p->width() / 2 - ((tlw + START_MARGINS + 25 + START_MARGINS + trw) / 2),
                          (Parsec.y + Parsec.h) + MAIN_MARGIN_TOP, tlw, tlh };
    SDL_Rect TextRight = { TextLeft.x + TextLeft.w + START_MARGINS + 25 + START_MARGINS, TextLeft.y, trw, trh };
    SDL_Rect Start = { TextLeft.x + TextLeft.w + START_MARGINS, TextLeft.y - 3, 25, 25 };

    /// RPIMore
    SDL_Rect RPIMore = { p->width() - (120 + 20), p->height() - (40 + 20), 120, 40 };
    SDL_Rect Expert = { RPIMore.x + 12, RPIMore.y + 5, 30, 30 };
    SDL_Rect Poweroff = { Expert.x + 35, Expert.y, 30, 30 };
    SDL_Rect Reboot = { Poweroff.x + 35, Expert.y, 30, 30 };

    /// Error
    SDL_QueryTexture(noServerError, nullptr, nullptr, &tlw, &tlh);
    SDL_Rect NoServerError = { p->width() / 2 - tlw / 2, p->height() / 2 - tlh / 2, tlw, tlh };

    /// Waking
    SDL_Rect Waking = { 50, 50, 50, 20 };

    mBounds[ELEM_PARSEC] = Parsec;
    mBounds[ELEM_RPIMORE] = RPIMore;
    mBounds[ELEM_EXPERT] = Expert;
    mBounds[ELEM_POWEROFF] = Poweroff;
    mBounds[ELEM_REBOOT] = Reboot;
    mBounds[ELEM_MAIN] = { TextLeft.x, TextLeft.y, (TextLeft.w + START_MARGINS + 25 + START_MARGINS + TextRight.w), TextRight.h };
    mBounds[ELEM_MAIN_TEXT_LEFT] = TextLeft;
    mBounds[ELEM_MAIN_TEXT_RIGHT] = TextRight;
    mBounds[ELEM_START] = Start;
    mBounds[ELEM_ERR_NOSERVERS] = NoServerError;
    mBounds[ELEM_WAKING] = Waking;
}

void Main::paint(Pirsec *p) {
    if (first) {
        fetchServers();
        first = false;
    }

    SDL_Renderer *r = p->renderer().get();
    SDL_Texture *serversText = get(TEXT_SERVERS)->getTexture().get();
    const SDL_Rect &rpimore = mBounds[ELEM_RPIMORE];
    const SDL_Rect &expert = mBounds[ELEM_EXPERT];
    const SDL_Rect &poweroff = mBounds[ELEM_POWEROFF];
    const SDL_Rect &reboot = mBounds[ELEM_REBOOT];

    /////////////////////////////////////////////// Dynamic layout /////////////////////////////////////////////////////
    if (mReset) {
        /// Background
        if (background != nullptr) {
            int bw = 0, bh = 0;
            SDL_QueryTexture(background, nullptr, nullptr, &bw, &bh);
            SDL_Rect Background;
            if (bw > bh) {
                int offset = std::max(0, bw - p->width());
                Background = { pirsec->width() / 2 - (bw - offset) / 2, pirsec->height() / 2 - bh / 2, p->width(), bh };
            } else {
                int offset = std::max(0, bh - p->height());
                Background = { pirsec->width() / 2 - bw / 2, pirsec->height() / 2 - (bh - offset) / 2, bw, p->height() };
            }
            mBounds[ELEM_BACKGROUND] = Background;
        }

        /// Servers
        int tlw = 0, tlh = 0;
        SDL_QueryTexture(serversText, nullptr, nullptr, &tlw, &tlh);
        SDL_Rect Servers = { p->width() / 2 - ((40 + 20 + tlw + 20) / 2), p->height() - (29 + 20), 40, 29 };
        SDL_Rect Left = { Servers.x + Servers.w, Servers.y + (Servers.h / 2 - 10), 20, 20 };
        SDL_Rect ServersText = { Left.x + Left.w, Servers.y + (Servers.h / 2 - (tlh / 2)), tlw, tlh };
        SDL_Rect Right = { ServersText.x + ServersText.w, Left.y, 20, 20 };

        mBounds[ELEM_SERVERS] = Servers;
        mBounds[ELEM_LEFT] = Left;
        mBounds[ELEM_SERVERS_TEXT] = ServersText;
        mBounds[ELEM_RIGHT] = Right;
    }

    /// Help
    dge::AnimatedValue &helpOpacityAnim = get(ANIM_HELP_RPIMORE_OPACITY)->getAnimatedValue();
    dge::AnimatedValue &helpYAnim = get(ANIM_HELP_Y)->getAnimatedValue();

    if (mReset || helpYAnim.isRunning() || helpOpacityAnim.isRunning()) {
        auto yOffset = -static_cast<Uint8>(helpYAnim.value());
        SDL_Rect HelpX = { expert.x + (expert.w / 2 - 12), rpimore.y - 35 - 5 + yOffset, 24, 35 };
        SDL_Rect X = { HelpX.x + 2, HelpX.y + 5, 20, 20 };
        SDL_Rect HelpB = { poweroff.x + (poweroff.w / 2 - 12), rpimore.y - 35 - 5 + yOffset, 24, 35 };
        SDL_Rect B = { HelpB.x + 2, HelpB.y + 5, 20, 20 };
        SDL_Rect HelpY = { reboot.x + (reboot.w / 2 - 12), rpimore.y - 35 - 5 + yOffset, 24, 35 };
        SDL_Rect Y = { HelpY.x + 2, HelpY.y + 5, 20, 20 };
        SDL_Rect ServersHelp = { mBounds[ELEM_SERVERS_TEXT].x + (mBounds[ELEM_SERVERS_TEXT].w / 2 - 30),
                                 mBounds[ELEM_SERVERS].y - 34 + yOffset, 60, 36 };
        SDL_Rect RB = { ServersHelp.x + (ServersHelp.w / 2 + 3), ServersHelp.y + 5, 20, 20 };
        SDL_Rect LB = { ServersHelp.x + (ServersHelp.w / 2 - 20 - 3), ServersHelp.y + 5, 20, 20 };

        mBounds[ELEM_HELPX] = HelpX;
        mBounds[ELEM_X] = X;
        mBounds[ELEM_HELPB] = HelpB;
        mBounds[ELEM_B] = B;
        mBounds[ELEM_HELPY] = HelpY;
        mBounds[ELEM_Y] = Y;
        mBounds[ELEM_SERVERS_HELP] = ServersHelp;
        mBounds[ELEM_RB] = RB;
        mBounds[ELEM_LB] = LB;
    }

    ///////////////////////////////////////////// Opacity Modulation ///////////////////////////////////////////////////
    auto screenOpacity = static_cast<Uint8>(get(Screen::ANIM_SCREEN_OPACITY)->getAnimatedValue().value());
    auto screenOpacityModulator = screenOpacity / 255.0;
    auto noServerOpacity = static_cast<Uint8>(get(ANIM_NO_SERVER_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);

    Uint8 backgroundOpacity;
    if (localBackground)
        backgroundOpacity = static_cast<Uint8>(get(ANIM_BACKGROUND_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);
    else
        backgroundOpacity = static_cast<Uint8>(255 - get(ANIM_BACKGROUND_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);

    auto mainOpacity = static_cast<Uint8>(get(ANIM_MAIN_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);
    auto mainOpacityModulator = mainOpacity / 255.0;
    auto mainTextOpacity = static_cast<Uint8>(get(ANIM_MAIN_TEXT)->getAnimatedValue().value() * mainOpacityModulator);

    auto helpRpiOpacity = static_cast<Uint8>(get(ANIM_HELP_RPIMORE_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);
    auto expertHoveredOpacity = static_cast<Uint8>(get(ANIM_EXPERT_HOVERED)->getAnimatedValue().value() * screenOpacityModulator);
    auto poweroffHoveredOpacity = static_cast<Uint8>(get(ANIM_POWEROFF_HOVERED)->getAnimatedValue().value() * screenOpacityModulator);
    auto rebootHoveredOpacity = static_cast<Uint8>(get(ANIM_REBOOT_HOVERED)->getAnimatedValue().value() * screenOpacityModulator);

    auto serversOpacity = static_cast<Uint8>(get(ANIM_SERVERS_OPACITY)->getAnimatedValue().value() * screenOpacityModulator);
    auto serversOpacityModulator = serversOpacity / 255.0;
    auto serversHelpOpacity = static_cast<Uint8>(get(ANIM_HELP_SERVERS_OPACITY)->getAnimatedValue().value() * serversOpacityModulator);

    ////////////////////////////////////////////////// Main ////////////////////////////////////////////////////////////
    /// Background
    if (background != nullptr) {
        if(localBackground) {
            SDL_SetTextureAlphaMod(background, backgroundOpacity);
            SDL_RenderCopy(r, background, nullptr, &mBounds[ELEM_BACKGROUND]);
        } else {
            SDL_SetTextureAlphaMod(background, screenOpacity);
            SDL_RenderCopy(r, background, nullptr, &mBounds[ELEM_BACKGROUND]);
            SDL_SetRenderDrawColor(r, 37, 37, 63, backgroundOpacity);
            SDL_RenderFillRect(r, &mBounds[ELEM_SCREEN]);
        }
    } else {
        SDL_SetRenderDrawColor(r, 37, 37, 63, screenOpacity);
        SDL_RenderFillRect(r, &mBounds[ELEM_SCREEN]);
    }

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_PARSEC)->getTexture().get(), mainOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_PARSEC)->getTexture().get(), nullptr, &mBounds[ELEM_PARSEC]);

    SDL_SetTextureAlphaMod(get(TEXT_MAIN_LEFT)->getTexture().get(), mainTextOpacity);
    SDL_RenderCopy(r, get(TEXT_MAIN_LEFT)->getTexture().get(), nullptr, &mBounds[ELEM_MAIN_TEXT_LEFT]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_START)->getTexture().get(), mainTextOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_START)->getTexture().get(), nullptr, &mBounds[ELEM_START]);

    SDL_SetTextureAlphaMod(get(TEXT_MAIN_RIGHT)->getTexture().get(), mainTextOpacity);
    SDL_RenderCopy(r, get(TEXT_MAIN_RIGHT)->getTexture().get(), nullptr, &mBounds[ELEM_MAIN_TEXT_RIGHT]);

    //////////////////////////////////////////////// RPIMore ///////////////////////////////////////////////////////////
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_RPIMORE)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_RPIMORE)->getTexture().get(), nullptr, &rpimore);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_EXPERT)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_EXPERT)->getTexture().get(), nullptr, &expert);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_EXPERT_HOVERED)->getTexture().get(), expertHoveredOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_EXPERT_HOVERED)->getTexture().get(), nullptr, &expert);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_POWEROFF)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_POWEROFF)->getTexture().get(), nullptr, &poweroff);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_POWEROFF_HOVERED)->getTexture().get(), poweroffHoveredOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_POWEROFF_HOVERED)->getTexture().get(), nullptr, &poweroff);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_REBOOT)->getTexture().get(), screenOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_REBOOT)->getTexture().get(), nullptr, &reboot);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_REBOOT_HOVERED)->getTexture().get(), rebootHoveredOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_REBOOT_HOVERED)->getTexture().get(), nullptr, &reboot);

    //////////////////////////////////////////////// Servers ///////////////////////////////////////////////////////////
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_SERVERS)->getTexture().get(), serversOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_SERVERS)->getTexture().get(), nullptr, &mBounds[ELEM_SERVERS]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_LEFT)->getTexture().get(), serversOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_LEFT)->getTexture().get(), nullptr, &mBounds[ELEM_LEFT]);

    SDL_SetTextureAlphaMod(serversText, serversOpacity);
    SDL_RenderCopy(r, serversText, nullptr, &mBounds[ELEM_SERVERS_TEXT]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_RIGHT)->getTexture().get(), serversOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_RIGHT)->getTexture().get(), nullptr, &mBounds[ELEM_RIGHT]);

    ////////////////////////////////////////////////// Help ////////////////////////////////////////////////////////////
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), nullptr, &mBounds[ELEM_HELPX]);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_X)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_X)->getTexture().get(), nullptr, &mBounds[ELEM_X]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), nullptr, &mBounds[ELEM_HELPB]);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_B)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_B)->getTexture().get(), nullptr, &mBounds[ELEM_B]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_HELP_REGULAR)->getTexture().get(), nullptr, &mBounds[ELEM_HELPY]);
    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_Y)->getTexture().get(), helpRpiOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_Y)->getTexture().get(), nullptr, &mBounds[ELEM_Y]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_HELP_LARGE)->getTexture().get(), serversHelpOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_HELP_LARGE)->getTexture().get(), nullptr, &mBounds[ELEM_SERVERS_HELP]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_LB)->getTexture().get(), serversHelpOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_LB)->getTexture().get(), nullptr, &mBounds[ELEM_LB]);

    SDL_SetTextureAlphaMod(p->get(Pirsec::IMG_RB)->getTexture().get(), serversHelpOpacity);
    SDL_RenderCopy(r, p->get(Pirsec::IMG_RB)->getTexture().get(), nullptr, &mBounds[ELEM_RB]);

    ////////////////////////////////////////////////// Errors //////////////////////////////////////////////////////////
    SDL_SetTextureAlphaMod(get(TEXT_ERR_NOSERVERS)->getTexture().get(), noServerOpacity);
    SDL_RenderCopy(r, get(TEXT_ERR_NOSERVERS)->getTexture().get(), nullptr, &mBounds[ELEM_ERR_NOSERVERS]);

    ////////////////////////////////////////////////// Waking //////////////////////////////////////////////////////////
    if (waking) {
        SDL_SetTextureAlphaMod(pirsec->get(Pirsec::IMG_WAKING)->getTexture().get(), screenOpacity);
        SDL_RenderCopy(r, pirsec->get(Pirsec::IMG_WAKING)->getTexture().get(), nullptr, &mBounds[ELEM_WAKING]);
    }
}

Main::Main(Pirsec *p) : pirsec(p), hand(p->handCursor()), arrow(p->arrowCursor()) {
    set(ANIM_BACKGROUND_OPACITY, new Animated(0.0, 30.0, 0.8, dge::EASE_LINEAR));
    set(ANIM_MAIN_OPACITY, new Animated(0.0, 255.0, 0.8, dge::EASE_LINEAR));
    set(ANIM_EXPERT_HOVERED, new Animated(0.0, 255.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_POWEROFF_HOVERED, new Animated(0.0, 255.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_REBOOT_HOVERED, new Animated(0.0, 255.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_MAIN_TEXT, new Animated(0.0, 255.0, 0.8, dge::EASE_OUT_CIRC, true, true));
    set(ANIM_HELP_RPIMORE_OPACITY, new Animated(0.0, 0.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_HELP_SERVERS_OPACITY, new Animated(0.0, 0.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_HELP_Y, new Animated(15.0, 0.0, 0.4, dge::EASE_LINEAR));
    set(ANIM_NO_SERVER_OPACITY, new Animated(0.0, 255.0, 0.8, dge::EASE_LINEAR));
    set(ANIM_SERVERS_OPACITY, new Animated(0.0, 255.0, 0.8, dge::EASE_LINEAR));
    set(TEXT_MAIN_LEFT, new Text("Press ", p->font("RubikMain").get()));
    set(TEXT_MAIN_RIGHT, new Text(" to be awesome", p->font("RubikMain").get()));
    set(TEXT_SERVERS, new Text("Unknown", p->font("RubikMain").get()));
    set(TEXT_ERR_NOSERVERS, new Text("There is no servers available to connect to.", p->font("RubikMain").get()));
    get(ANIM_MAIN_TEXT)->getAnimatedValue().start();
    show();

    pirsec->interval([this](double now, double at) {
        fetchServers();
    }, std::chrono::seconds(20));
}

void Main::handle(const SDL_Event &evt) {
    if (evt.type == SDL_CONTROLLERBUTTONUP) {
        switch (evt.cbutton.button) {
            case SDL_CONTROLLER_BUTTON_B:
                runHook(POWEROFF, "");
                hideHelp();
                break;
            case SDL_CONTROLLER_BUTTON_Y:
                runHook(REBOOT, "");
                hideHelp();
                break;
            case SDL_CONTROLLER_BUTTON_DPAD_UP:
                fetchBackground();
                break;
            case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                // Our little secret ;)
                fetchServers();
                break;
            case SDL_CONTROLLER_BUTTON_START:
                runHook(START, this->servers[currentServer]["server_id"]);
                hideHelp();
                break;
            case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
                currentServer = std::max(currentServer - 1, 0);
                selectServer();
                hideHelp();
                break;
            case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
                currentServer = std::min(currentServer + 1, (const int &) (servers.size() - 1));
                selectServer();
                hideHelp();
                break;
            case SDL_CONTROLLER_BUTTON_BACK:
                runHook(WAKE, "");
                hideHelp();
                break;
            default:
                toggleHelp();
                break;
        }
    } else if (evt.type == SDL_KEYDOWN && (evt.key.keysym.mod & KMOD_ALT)) {
        switch (evt.key.keysym.sym) {
            case SDLK_p:
                runHook(POWEROFF, "");
                break;
            case SDLK_r:
                runHook(REBOOT, "");
                break;
            case SDLK_s:
                runHook(START, this->servers[currentServer]["server_id"]);
                break;
            case SDLK_w:
                runHook(WAKE, "");
                pirsec->disableScreenSaver();
                break;
            case SDLK_b:
                fetchBackground();
                break;
            case SDLK_f:
                // Our little secret ;)
                fetchServers();
                break;
            case SDLK_LEFT:
                currentServer = std::max(currentServer - 1, 0);
                selectServer();
                break;
            case SDLK_RIGHT:
                currentServer = std::min(currentServer + 1, (const int &) (servers.size() - 1));
                selectServer();
                break;
            default:
                break;
        }
    } else if (isShown() && (evt.type == SDL_MOUSEBUTTONDOWN || evt.type == SDL_MOUSEMOTION)) {
        if (evt.type == SDL_MOUSEBUTTONDOWN && evt.button.button == SDL_BUTTON_MIDDLE) {
            fetchBackground();
            return;
        }

        dge::AnimatedValue &expert = get(ANIM_EXPERT_HOVERED)->getAnimatedValue();
        dge::AnimatedValue &poweroff = get(ANIM_POWEROFF_HOVERED)->getAnimatedValue();
        dge::AnimatedValue &reboot = get(ANIM_REBOOT_HOVERED)->getAnimatedValue();
        SDL_Point mousePosition = {evt.motion.x, evt.motion.y};
        bool expertHovered = hovering(ELEM_EXPERT, mousePosition),
                poweroffHovered = hovering(ELEM_POWEROFF, mousePosition),
                rebootHovered = hovering(ELEM_REBOOT, mousePosition),
                mainHovered = hovering(ELEM_MAIN, mousePosition),
                leftHovered = hovering(ELEM_LEFT, mousePosition),
                rightHovered = hovering(ELEM_RIGHT, mousePosition);

        if (mainHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) runHook(START, this->servers[currentServer]["server_id"]);
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
        } else if (expertHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) pirsec->stop();
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
            if (expert.value() != 255 && !(expert.getEndValue() == 255 && expert.isRunning())) {
                expert.setStartValue(expert.value());
                expert.setEndValue(255);
                expert.start();
            }
        } else if (poweroffHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) runHook(POWEROFF, "");
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
            if (poweroff.value() != 255 && !(poweroff.getEndValue() == 255 && poweroff.isRunning())) {
                poweroff.setStartValue(poweroff.value());
                poweroff.setEndValue(255);
                poweroff.start();
            }
        } else if (rebootHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) (evt.button.button == SDL_BUTTON_RIGHT) ? pirsec->restart() : runHook(REBOOT, "");
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
            if (reboot.value() != 255 && !(reboot.getEndValue() == 255 && reboot.isRunning())) {
                reboot.setStartValue(reboot.value());
                reboot.setEndValue(255);
                reboot.start();
            }
        } else if (leftHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) {
                currentServer = std::max(currentServer - 1, 0);
                selectServer();
            }
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
        } else if (rightHovered) {
            if (evt.type == SDL_MOUSEBUTTONDOWN) {
                currentServer = std::min(currentServer + 1, (const int &) (servers.size() - 1));
                selectServer();
            }
            if (SDL_GetCursor() == arrow.get())
                SDL_SetCursor(hand.get());
        } else {
            if (SDL_GetCursor() != arrow.get())
                SDL_SetCursor(arrow.get());
        }

        if (!expertHovered && expert.value() != 0 && !(expert.getEndValue() == 0 && expert.isRunning())) {
            expert.setStartValue(expert.value());
            expert.setEndValue(0);
            expert.start();
        }

        if (!poweroffHovered && poweroff.value() != 0 && !(poweroff.getEndValue() == 0 && poweroff.isRunning())) {
            poweroff.setStartValue(poweroff.value());
            poweroff.setEndValue(0);
            poweroff.start();
        }

        if (!rebootHovered && reboot.value() != 0 && !(reboot.getEndValue() == 0 && reboot.isRunning())) {
            reboot.setStartValue(reboot.value());
            reboot.setEndValue(0);
            reboot.start();
        }
    } else if (evt.type == pirsec->PIRSEC_EVENTS && evt.user.code == Pirsec::PIRSEC_EVENTS_GET_URL_RESULTS) {
        dge::AnimatedValue &main = get(ANIM_MAIN_OPACITY)->getAnimatedValue();
        dge::AnimatedValue &servers = get(ANIM_SERVERS_OPACITY)->getAnimatedValue();
        try {
            std::string *result = ((std::string *) evt.user.data1);
            if (*result != "Session invalid.") {
                setServers(*result);
                hideNoServer();

                if (main.value() != 255) {
                    main.setStartValue(main.value());
                    main.setEndValue(255);
                    main.start();
                }

                if (servers.value() != 255) {
                    servers.setStartValue(servers.value());
                    servers.setEndValue(255);
                    servers.start();
                }
            } else {
                pirsec->removeCredentials();
            }
        } catch (...) {
            showNoServer();
        }
    } else if (evt.type == pirsec->PIRSEC_EVENTS && evt.user.code == Pirsec::PIRSEC_EVENTS_GET_URL_IMG_RESULTS) {
        std::string *filename = ((std::string *) evt.user.data1);
        if (background != nullptr) SDL_DestroyTexture(background);
        background = IMG_LoadTexture(pirsec->renderer().get(), filename->c_str());

        if (background == nullptr) std::cout << "Texture can't be loaded : " << SDL_GetError() << std::endl;

        if (std::remove(filename->c_str()) != 0) std::cout << "/!\\ Can't delete the temporary file" << std::endl;
        get(ANIM_BACKGROUND_OPACITY)->getAnimatedValue().start();
        reset();
    }
}

void Main::initialize(Pirsec *p) {
    Screen::initialize(p);
    fetchBackground();
}
