#include "../Headers/Image.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdexcept>
#include <string>

#include "../Headers/Pirsec.hpp"

Image::Image(const std::string &path) : mPath(path) {}

void Image::load(Pirsec *p) {
    this->mTexture = std::unique_ptr<SDL_Texture, Utils::SDL_Deleter>(IMG_LoadTexture(p->renderer().get(), this->mPath.c_str()));
    if (!this->mTexture) {
        throw std::runtime_error(std::string("Unable to load image " + this->mPath + " ! SDL_image Error: ").append(IMG_GetError()));
    }
}

const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &Image::getTexture() {
    return this->texture();
}

const std::unique_ptr<SDL_Texture, Utils::SDL_Deleter> &Image::texture() {
    return this->mTexture;
}

const std::string &Image::path() {
    return this->mPath;
}
