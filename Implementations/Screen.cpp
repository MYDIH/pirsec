#include "../Headers/Screen.hpp"

#include "../Headers/Animated.hpp"
#include "../Headers/Pirsec.hpp"

void Screen::layout(Pirsec *p) {
    SDL_Rect Screen = { 0, 0, p->width(), p->height() };
    mBounds[ELEM_SCREEN] = Screen;
}

bool Screen::hovering(int element, const SDL_Point &position) {
    try {
        const SDL_Rect &e = mBounds.at(element);
        return position.x > e.x && position.y > e.y &&
               position.x < e.x + e.w && position.y < e.y + e.h;
    } catch (const std::out_of_range &ex) {
        return false;
    }
}

Screen::Screen() {
    set(ANIM_SCREEN_OPACITY, new Animated(0.0, 255.0, 0.8, dge::EASE_LINEAR));
}

void Screen::initialize(Pirsec *p) { load(p); }

void Screen::render(Pirsec *p) {
    if (!isHidden()) {
        if (mReset) {
            layout(p);
            paint(p);
            mReset = false;
        } else paint(p);
    }
}

void Screen::reset()
{ mReset = true; }

void Screen::show() {
    dge::AnimatedValue &screen = get(ANIM_SCREEN_OPACITY)->getAnimatedValue();
    screen.setStartValue(screen.value());
    screen.setEndValue(255.0);
    screen.start();
}

void Screen::hide() {
    dge::AnimatedValue &screen = get(ANIM_SCREEN_OPACITY)->getAnimatedValue();
    screen.setStartValue(screen.value());
    screen.setEndValue(0.0);
    screen.start();
}

bool Screen::isHidden()
{ return get(ANIM_SCREEN_OPACITY)->getAnimatedValue().value() == 0; }

bool Screen::isShown()
{ return get(ANIM_SCREEN_OPACITY)->getAnimatedValue().value() == 255; }


