#include "../Headers/ResourceHandler.hpp"

#include <iostream>

ResourceHandler& ResourceHandler::set(int index, Resource *res) {
    mResources[index] = std::unique_ptr<Resource>(res);
    return *this;
}

const std::unique_ptr<Resource> &ResourceHandler::get(int index) {
    return mResources[index];
}

void ResourceHandler::load(Pirsec *p) const {
    for(const auto &res : mResources) {
        res.second->load(p);
        // std::cout << "Resource named :[" << res.first << "] initialized" << std::endl;
    }
}
