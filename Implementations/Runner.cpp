#include "../Headers/Runner.hpp"
#include "../Headers/Pirsec.hpp"

#include <SDL2/SDL_events.h>
#include <curl/curl.h>
#include <fstream>

void Runner::run(Pirsec *p) {
    SDL_Event evt;
    bool quit = false;
    std::string result;
    std::string resultImg;
    while (!quit) {
        // Ok that's not cool, the two threads may disturb each others. Will do the trick for now
        // TODO: implement inter-process communication or find a way to call get() only when a future is done
        while (SDL_PeepEvents(&evt, 1, SDL_PEEKEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT) > 0 && !quit) {
            if (evt.type == p->PIRSEC_EVENTS) {
                switch (evt.user.code) {
                    case Pirsec::PIRSEC_EVENTS_STOP: {
                        SDL_PeepEvents(&evt, 1, SDL_GETEVENT, p->PIRSEC_EVENTS, p->PIRSEC_EVENTS);
                        quit = true;
                        break;
                    }
                    case Pirsec::PIRSEC_EVENTS_GET_URL: {
                        SDL_PeepEvents(&evt, 1, SDL_GETEVENT, p->PIRSEC_EVENTS, p->PIRSEC_EVENTS);
                        struct curl_slist *headers = NULL;
                        auto url = ((std::string *) evt.user.data1);
                        auto header = ((std::string *) evt.user.data2);
                        result.clear();

                        if (header != nullptr)
                            headers = curl_slist_append(headers, header->c_str());

                        CURL *curlHandle = curl_easy_init();
                        curl_easy_setopt(curlHandle, CURLOPT_URL, url->c_str());
                        curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, Utils::CURL_WRITE_TO_STRING_CALLBACK);
                        curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);
                        curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &result);
                        CURLcode res = curl_easy_perform(curlHandle);
                        if (res != CURLE_OK) {
                            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                                    curl_easy_strerror(res));
                        }
                        curl_easy_cleanup(curlHandle);
                        curl_slist_free_all(headers);
                        p->pushPirsecEvent(Pirsec::PIRSEC_EVENTS_GET_URL_RESULTS, &result, NULL);
                        break;
                    }
                    case Pirsec::PIRSEC_EVENTS_GET_URL_IMG: {
                        SDL_PeepEvents(&evt, 1, SDL_GETEVENT, p->PIRSEC_EVENTS, p->PIRSEC_EVENTS);
                        auto url = ((std::string *) evt.user.data1);
                        auto id = ((std::string *) evt.user.data2);

                        size_t pos = url->find("$i");
                        std::string replacedUrl = std::string(*url);
                        if (pos != std::string::npos)
                            replacedUrl.replace(pos, 2, *id);

                        resultImg = "/tmp/" + std::to_string(dge::Timeline::getCurrentTime()) + ".jpg";
                        FILE *fp = fopen(resultImg.c_str(), "wb");
                        CURL *curlHandle = curl_easy_init();
                        curl_easy_setopt(curlHandle, CURLOPT_URL, replacedUrl.c_str());
                        curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, NULL);
                        curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, fp);
                        CURLcode res = curl_easy_perform(curlHandle);
                        if (res != CURLE_OK)
                            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                                    curl_easy_strerror(res));
                        fclose(fp);

                        curl_easy_cleanup(curlHandle);
                        p->pushPirsecEvent(Pirsec::PIRSEC_EVENTS_GET_URL_IMG_RESULTS, &resultImg, NULL);
                        break;
                    }
                    default:
                        break;
                }
            }
        }
        SDL_Delay(500); // keep processor happy
    }
}
