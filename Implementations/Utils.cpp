#include <string>
#include <vector>

#include "../Headers/Utils.hpp"

void Utils::SDL_Deleter::operator()(SDL_Window *p) const { SDL_DestroyWindow(p); }
void Utils::SDL_Deleter::operator()(SDL_Renderer *p) const { SDL_DestroyRenderer(p); }
void Utils::SDL_Deleter::operator()(SDL_Texture *p) const { SDL_DestroyTexture(p); }
void Utils::SDL_Deleter::operator()(SDL_Cursor *p) const { SDL_FreeCursor(p); }
void Utils::SDL_Deleter::operator()(SDL_GameController *p) const { SDL_GameControllerClose(p); }
void Utils::SDL_Deleter::operator()(TTF_Font *p) const { TTF_CloseFont(p); }

size_t Utils::CURL_WRITE_TO_STRING_CALLBACK(char *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string*) userp)->append(contents, size * nmemb);
    return size * nmemb;
}

// NOTE: This isn't used because of a RPI quirk ... IMG_LoadTextureRaw sefaults when an error occur
// in an external lib and libjpeg is doing weird things on RPI
size_t Utils::CURL_WRITE_TO_MEMORY_CALLBACK(char *contents, size_t size, size_t nmemb, void *userp) {
    std::vector<char> *buff = ((std::vector<char>*) userp);
    buff->insert(buff->end(), contents, contents + (size * nmemb));
    return size * nmemb;
}