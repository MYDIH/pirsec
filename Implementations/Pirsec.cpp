#include "../Headers/Pirsec.hpp"

#include <SDL2/SDL.h>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include "../Headers/screens/Main.hpp"
#include "../Headers/screens/Login.hpp"
#include "../Headers/screens/ScreenSaver.hpp"

#include "../Headers/imgs/B.hpp"
#include "../Headers/imgs/Y.hpp"
#include "../Headers/imgs/X.hpp"
#include "../Headers/imgs/RB.hpp"
#include "../Headers/imgs/LB.hpp"
#include "../Headers/imgs/Start.hpp"
#include "../Headers/imgs/Parsec.hpp"
#include "../Headers/imgs/Servers.hpp"
#include "../Headers/imgs/Expert.hpp"
#include "../Headers/imgs/Poweroff.hpp"
#include "../Headers/imgs/Reboot.hpp"
#include "../Headers/imgs/ExpertHovered.hpp"
#include "../Headers/imgs/PoweroffHovered.hpp"
#include "../Headers/imgs/RebootHovered.hpp"
#include "../Headers/imgs/RPIMore.hpp"
#include "../Headers/imgs/HelpRegular.hpp"
#include "../Headers/imgs/Left.hpp"
#include "../Headers/imgs/Right.hpp"
#include "../Headers/imgs/HelpLarge.hpp"
#include "../Headers/imgs/PirsecLogo.hpp"
#include "../Headers/imgs/Waking2.hpp"

void Pirsec::setSize() {
    if (mWidth == -1 || mHeight == -1) {
        SDL_GetRendererOutputSize(mRenderer.get(), &mWidth, &mHeight);
    }
}

void Pirsec::getCredentials() {
    std::string home = std::getenv("HOME");
    std::ifstream pirsecAuthfile(home + "/.pirsec/auth");
    std::ifstream parsecAuthfile(home + "/.parsec/auth");
    std::string token;

    if (pirsecAuthfile.is_open()) {
        std::getline(pirsecAuthfile, token);
    } else if (parsecAuthfile.is_open()) {
        std::getline(parsecAuthfile, token);
    }
    mToken = token;
}

void Pirsec::saveCredentials(const std::string &token) {
    std::string home = std::getenv("HOME");
    std::ofstream pirsecAuthfile(home + "/.pirsec/auth", std::ios::out | std::ios::trunc);
    if (pirsecAuthfile.is_open()) {
        pirsecAuthfile << token << std::endl;
    } else {
        throw std::runtime_error("Can't save the credentials");
    }

    mToken = token;

    Main *main = static_cast<Main*>(mScreens["Main"].get());

    SDL_StopTextInput();
    mScreens["Login"]->hide();
    main->fetchServers();
    main->show();
}

void Pirsec::removeCredentials() {
    std::string home = std::getenv("HOME");
    std::string cred = home + "/.pirsec/auth";
    if (std::remove(cred.c_str()) != 0) {
        std::cout << "Can't erase the credentials under the ~/.pirsec directory. Please proceed manually" << std::endl;
    } /* else {
        std::cout << "Credentials have been removed." << std::endl;
    } */
    mToken = "";
    SDL_StartTextInput();
    mScreens["Main"]->hide();
    mScreens["Login"]->show();
}

void Pirsec::scheduleScreenSaver() {
    mScreenSaveTask = schedule([this](double now, double at) {
        mScreens["ScreenSaver"]->show();
        mScreens["Login"]->hide();
        mScreens["Main"]->hide();
    }, std::chrono::seconds(30));
}

void Pirsec::render() {
    SDL_SetRenderDrawColor(this->mRenderer.get(), 37, 37, 63, 255);
    SDL_RenderClear(this->mRenderer.get());

    mScreens["Login"]->render(this);
    mScreens["Main"]->render(this);
    mScreens["ScreenSaver"]->render(this); // order matters

    SDL_RenderPresent(this->mRenderer.get());
}

void Pirsec::handle() {
    SDL_Event evt;
    SDL_PumpEvents();

    bool sshandled = false, mousehandled = false;
    while (SDL_PeepEvents(&evt, 1, SDL_PEEKEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT) > 0) {
        if (evt.type != PIRSEC_EVENTS || evt.user.code == PIRSEC_EVENTS_GET_URL_RESULTS ||
            evt.user.code == PIRSEC_EVENTS_GET_URL_IMG_RESULTS) {
            SDL_PeepEvents(&evt, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT);
        }

        if (evt.type != PIRSEC_EVENTS && !mScreenSaveCanceled && !sshandled) {
            Main *main = static_cast<Main*>(mScreens["Main"].get());

            if (mScreens["ScreenSaver"]->isShown()) {
                main->fetchBackground();
                mScreens["ScreenSaver"]->hide();
                if (mToken.empty()) mScreens["Login"]->show();
                else main->show();
            }

            // KISS, Tried to cancel only when needed,
            // takes too long time for me to figure out how.
            cancel(mScreenSaveTask);
            scheduleScreenSaver();
            sshandled = true;
        }

        switch (evt.type) {
            case SDL_QUIT: {
                this->stop();
                break;
            }
            case SDL_WINDOWEVENT: {
                switch (evt.window.event) {
                    case SDL_WINDOWEVENT_RESIZED:
                        this->reset();
                        break;
                    default:
                        break;
                }
                break;
            } case SDL_MOUSEMOTION: {
                if (!mousehandled) {
                    SDL_ShowCursor(SDL_ENABLE);

                    // KISS, Tried to cancel only when needed,
                    // takes too long time for me to figure out how.
                    cancel(mHideCursorTask);
                    mHideCursorTask = schedule([](double now, double at) {
                        SDL_ShowCursor(SDL_DISABLE);
                    }, std::chrono::seconds(5));
                    mousehandled = true;
                }
                break;
            } case SDL_KEYDOWN: {
                if (evt.key.keysym.sym == SDLK_r && (evt.key.keysym.mod & KMOD_SHIFT) && (evt.key.keysym.mod & KMOD_ALT)) {
                    // Clear the event loop
                    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                    restart();
                    return;
                } else if (evt.key.keysym.sym == SDLK_x && (evt.key.keysym.mod & KMOD_ALT)) {
                    // Clear the event loop
                    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                    stop();
                    return;
                }
                break;
            } case SDL_CONTROLLERBUTTONDOWN: {
                if (evt.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN) dpaddown = true;
                break;
            } case SDL_CONTROLLERBUTTONUP: {
                if (evt.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN) dpaddown = false;
                else if (evt.cbutton.button == SDL_CONTROLLER_BUTTON_Y && dpaddown) {
                    // Clear the event loop
                    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                    restart();
                    return;
                }
                else if (evt.cbutton.button == SDL_CONTROLLER_BUTTON_X) {
                    // Clear the event loop
                    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                    stop();
                    return;
                }
                break;
            } case SDL_CONTROLLERDEVICEADDED: {
                if (SDL_IsGameController(evt.jdevice.which)) {
                    SDL_GameController* controller = SDL_GameControllerOpen(evt.jdevice.which);
                    // std::cout << "New controller \"" << SDL_GameControllerName(controller) << "\" detected." << std::endl;
                }
                break;
            } default: break;
        }

        for (const auto &screen : mScreens) {
            // TODO: This could be more granular
            if(!screen.second->isHidden()) screen.second->handle(evt);
        }
    }
}

void Pirsec::reset() {
    mWidth = mHeight = -1;
    for (const auto &screen : mScreens) {
        screen.second->reset();
    }
}

Pirsec::Pirsec() : mHandCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND)),
    mArrowCursor(SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW)) {
    mFonts.insert(std::make_pair("RubikMain", std::unique_ptr<TTF_Font, Utils::SDL_Deleter>(TTF_OpenFont("Resources/Rubik.ttf", 14))));

    bool fontInitError = false;
    for (const auto &font : mFonts) {
        if (font.second == NULL)
            fontInitError = true;
    }
    if (fontInitError) throw std::runtime_error(std::string("Can't initialize my fonts -> ").append(TTF_GetError()));

    set(IMG_B, new B());
    set(IMG_Y, new Y());
    set(IMG_X, new X());
    set(IMG_RB, new RB());
    set(IMG_LB, new LB());
    set(IMG_START, new Start());
    set(IMG_PARSEC, new Parsec());
    set(IMG_SERVERS, new Servers());
    set(IMG_EXPERT, new Expert());
    set(IMG_EXPERT_HOVERED, new ExpertHovered());
    set(IMG_POWEROFF, new Poweroff());
    set(IMG_POWEROFF_HOVERED, new PoweroffHovered());
    set(IMG_REBOOT, new Reboot());
    set(IMG_REBOOT_HOVERED, new RebootHovered());
    set(IMG_RPIMORE, new RPIMore());
    set(IMG_HELP_REGULAR, new HelpRegular());
    set(IMG_HELP_LARGE, new HelpLarge());
    set(IMG_LEFT, new Left());
    set(IMG_RIGHT, new Right());
    set(IMG_PIRSEC_LOGO, new PirsecLogo());
    set(IMG_WAKING, new Waking2());

    mScreens.insert(std::make_pair("Main", std::unique_ptr<Screen>(new Main(this))));
    mScreens.insert(std::make_pair("Login", std::unique_ptr<Screen>(new Login(this))));
    mScreens.insert(std::make_pair("ScreenSaver", std::unique_ptr<Screen>(new ScreenSaver())));

    initialize();
}

void Pirsec::initialize() {
    mWindow = std::unique_ptr<SDL_Window, Utils::SDL_Deleter>(
            SDL_CreateWindow("Pirsec", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 800, SDL_WINDOW_RESIZABLE));
    if (!mWindow) { throw std::runtime_error(std::string("Can't initialize the Window : ").append(SDL_GetError())); }
    mRenderer = std::unique_ptr<SDL_Renderer, Utils::SDL_Deleter>(SDL_CreateRenderer(this->mWindow.get(), -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED));
    if (!mRenderer) { throw std::runtime_error(std::string("Can't initialize the Renderer : ").append(SDL_GetError())); }

    /////////////////////////////////////// Initializing Pircsec custom events /////////////////////////////////////////
    Uint32 evtType = SDL_RegisterEvents(1);
    if (evtType != ((Uint32)-1))
        PIRSEC_EVENTS = evtType;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    getCredentials();
    load(this);

    // Initializing the Screens (will load the resources needed for each screens)
    for (const auto &screen : mScreens) {
        screen.second->initialize(this);
    }

    if (mToken.empty()) {
        SDL_StartTextInput();
        mScreens["Login"]->show();
        mScreens["Main"]->hide();
    } else {
        mScreens["Login"]->hide();
        mScreens["Main"]->show();
    }

    scheduleScreenSaver();

    SDL_ShowCursor(SDL_DISABLE);
    SDL_SetRenderDrawBlendMode(mRenderer.get(), SDL_BLENDMODE_BLEND);
    mInitialized = true;
}

bool Pirsec::restarting()
{ return this->mRestarting; }

bool Pirsec::running()
{ return this->mRunning; }

void Pirsec::run() {
    // This call hangs for more than a second on start
    // TODO: Inspect this bug
    if (mInitialized) {
        handle();
        tick();
        render();
    }
}

int Pirsec::width() {
    setSize();
    return mWidth;
}

int Pirsec::height() {
    setSize();
    return mHeight;
}

void Pirsec::restart() {
    mRestarting = true;
    mRunning = false;
}

void Pirsec::stop()
{ mRunning = false; }

const std::string &Pirsec::getToken()
{ return mToken; }

void Pirsec::setToken(const std::string &token) {
    saveCredentials(token);
    mToken = token;
}

void Pirsec::pushPirsecEvent(const Sint32 code, void *data1, void *data2) const {
    SDL_Event event;
    SDL_zero(event);
    event.type = PIRSEC_EVENTS;
    event.user.code = code;
    event.user.data1 = data1;
    event.user.data2 = data2;
    SDL_PushEvent(&event);
}

void Pirsec::erase() {
    mRenderer.reset(nullptr);
    mWindow.reset(nullptr);
    mInitialized = false;
}

const std::unique_ptr<SDL_Renderer, Utils::SDL_Deleter> &Pirsec::renderer() const
{ return this->mRenderer; }

const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &Pirsec::handCursor() const
{ return mHandCursor; }

const std::unique_ptr<SDL_Cursor, Utils::SDL_Deleter> &Pirsec::arrowCursor() const
{ return mArrowCursor; }

const std::unique_ptr<TTF_Font, Utils::SDL_Deleter> &Pirsec::font(const std::string &id) const
{ return mFonts.at(id); }

void Pirsec::disableScreenSaver() {
    mScreenSaveCanceled = true;
    cancel(mScreenSaveTask);
}

void Pirsec::enableScreenSaver() {
    mScreenSaveCanceled = false;
    scheduleScreenSaver();
}
