#include "../Headers/Animated.hpp"
#include "../Headers/Pirsec.hpp"

Animated::Animated(float startValue, float endValue, double duration, const dge::Easing &easing,
        bool loop, bool pingPong) : AnimatedValue(startValue, endValue, duration, easing, loop, pingPong) {}

Animated::Animated(const dge::AnimatedValue &a) : AnimatedValue(a) {}

void Animated::load(Pirsec *p)
{ this->activate(p); }

dge::AnimatedValue & Animated::getAnimatedValue()
{ return *this; }
