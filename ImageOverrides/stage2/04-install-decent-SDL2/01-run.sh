#!/bin/bash -e

rm -rf "${ROOTFS_DIR}/tmp/SDL2"
mkdir "${ROOTFS_DIR}/tmp/SDL2"
tar xzf "files/SDL2-2.0.8.tar.gz" -C "${ROOTFS_DIR}/tmp/SDL2"

rm -rf "${ROOTFS_DIR}/tmp/SDL2_image"
mkdir "${ROOTFS_DIR}/tmp/SDL2_image"
tar xzf "files/SDL2_image-2.0.3.tar.gz" -C "${ROOTFS_DIR}/tmp/SDL2_image"

rm -rf "${ROOTFS_DIR}/tmp/SDL2_ttf"
mkdir "${ROOTFS_DIR}/tmp/SDL2_ttf"
tar xzf "files/SDL2_ttf-NoGl-2.0.14.tar.gz" -C "${ROOTFS_DIR}/tmp/SDL2_ttf"

echo "Building SDL2"
on_chroot << EOF
cd /tmp/SDL2
mkdir build && cd build
../configure --host=arm-raspberry-linux-gnueabihf --disable-pulseaudio --disable-esd --disable-video-mir --disable-video-wayland --disable-video-x11 --disable-video-opengl
make -j 4
make install
usermod -aG input pi
usermod -aG tty pi
rm -rf /tmp/SDL2
cd /tmp/SDL2_image
mkdir build && cd build
../configure
make -j 4
make install
rm -rf /tmp/SDL2_image
cd /tmp/SDL2_ttf
mkdir build && cd build
../configure --without-x --disable-sdltest
make -j 4
make install
rm -rf /tmp/SDL2_ttf
EOF
echo "SDL2 built"
