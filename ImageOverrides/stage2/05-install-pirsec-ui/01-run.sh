#!/bin/bash -e

rm -rf "${ROOTFS_DIR}/tmp/Pirsec"
mkdir "${ROOTFS_DIR}/tmp/Pirsec"
tar xzf "files/Pirsec.tar.gz" -C "${ROOTFS_DIR}/tmp/Pirsec"
cp "files/.profile" "${ROOTFS_DIR}/home/pi/"

echo "Building Pirsec"
on_chroot << EOF
rm -f /usr/bin/pirsec
rm -rf /opt/Pirsec

mv /tmp/Pirsec /opt/
cd /opt/Pirsec
ln -s /opt/Pirsec/Pirsec.sh /usr/bin/pirsec
cmake CMakeLists.txt
make -j 4
wget https://s3.amazonaws.com/parsec-build/package/parsec-rpi.deb
dpkg -i parsec-rpi.deb
rm parsec-rpi.deb

# Configuring mouse poll interval
sed -i -e 's/ext4 elevator/ext4 usbhid.mousepoll=8 elevator/g' /boot/cmdline.txt

# Configuring memory split
echo "
# Memory split defined by the Pirsec installer. Use raspi-config to change it
gpu_mem_256=128
gpu_mem_512=256
gpu_mem_1024=512" >> /boot/config.txt

# Configuring autologin like here -> https://github.com/RPi-Distro/raspi-config/blob/f4e6d18751fd8e5fc69c0f0e8319a1018b92cfc7/raspi-config#L956
systemctl set-default multi-user.target
sed /etc/systemd/system/autologin@.service -i -e "s#^ExecStart=-/sbin/agetty --autologin [^[:space:]]*#ExecStart=-/sbin/agetty --autologin pi#"
ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service

# Prompt the user to configure on first start
touch /home/pi/.configure-me
EOF
echo "Pirsec built"
