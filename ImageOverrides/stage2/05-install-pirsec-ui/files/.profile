# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
      . "$HOME/.bashrc"
  fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi

if [ -f "$HOME/.configure-me" ]; then
  echo "

  It's the first time you launch this system. Raspi-config will now open to give
  you a chance to configure the beast (changing locale, keymap, wifi region). If
  you need to configure further (actual wifi connexion for instance), please, do
  not reboot if you are prompted to, and do the additional steps you need in the
  console. When done, simply reboot to see the UI. Welcome friend.

  I will sleep 5s"

  sleep 5

  rm -f "$HOME/.configure-me"
  sudo raspi-config
elif [ "$SSH_CONNECTION" == "" ]; then
  # Uncomment this to allow XboxOne Controllers to be pairable
  # sudo bash -c 'echo 1 > /sys/module/bluetooth/parameters/disable_ertm'
  pirsec
fi
